﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    GameObject Car;
    private bool followTarget;
    public float followAhead, smoothing;
    private Vector3 targetPosition;
    // Start is called before the first frame update
    void Start()
    {
        followTarget = true;
    }

    // Update is called once per frame
    void Update()
    {
        Car = GameObject.FindGameObjectWithTag("Car");
        if (followTarget)
        {
            targetPosition = new Vector3(Car.transform.position.x, Car.transform.position.y+12f, transform.position.z);

            if (Car.transform.localScale.x > 0f)
            {
                targetPosition = new Vector3(targetPosition.x + followAhead, targetPosition.y, targetPosition.z);
            }
            if (Car.transform.localScale.x < 0f)
            {
                targetPosition = new Vector3(targetPosition.x + followAhead, targetPosition.y, targetPosition.z);
            }

            transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing * Time.deltaTime);
            
        }
    }
}
