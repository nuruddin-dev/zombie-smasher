﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarWheel : MonoBehaviour
{
    public Collider2D wheelCollider;
    public float CarOneWheelFriction, CarTwoWheelFriction, CarThreeWheelFriction, CarFourWheelFriction, CarFiveWheelFriction;
    
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefsFriction();
        wheelCollider = GetComponent<Collider2D>();
        SetWheelFriction();
    }

    //get friction otherwise set the default value
    void PlayerPrefsFriction()
    {
        if (PlayerPrefs.HasKey("CarOneWheelFriction"))
        {
            Debug.Log("Car wheel cs is called");
            CarOneWheelFriction = PlayerPrefs.GetFloat("CarOneWheelFriction");
        }
        else
        {
            CarOneWheelFriction = .5f;
            PlayerPrefs.SetFloat("CarOneWheelFriction", CarOneWheelFriction);
        }
        if (PlayerPrefs.HasKey("CarTwoWheelFriction"))
        {
            CarTwoWheelFriction = PlayerPrefs.GetFloat("CarTwoWheelFriction");
        }
        else
        {
            CarTwoWheelFriction = .5f;
            PlayerPrefs.SetFloat("CarTwoWheelFriction", CarTwoWheelFriction);
        }
        if (PlayerPrefs.HasKey("CarThreeWheelFriction"))
        {
            CarThreeWheelFriction = PlayerPrefs.GetFloat("CarThreeWheelFriction");
        }
        else
        {
            CarThreeWheelFriction = .5f;
            PlayerPrefs.SetFloat("CarThreeWheelFriction", CarThreeWheelFriction);
        }
        if (PlayerPrefs.HasKey("CarFourWheelFriction"))
        {
            CarFourWheelFriction = PlayerPrefs.GetFloat("CarFourWheelFriction");
        }
        else
        {
            CarFourWheelFriction = .5f;
            PlayerPrefs.SetFloat("CarFourWheelFriction", CarFourWheelFriction);
        }
        if (PlayerPrefs.HasKey("CarFiveWheelFriction"))
        {
            CarFiveWheelFriction = PlayerPrefs.GetFloat("CarFiveWheelFriction");
        }
        else
        {
            CarFiveWheelFriction = .5f;
            PlayerPrefs.SetFloat("CarFiveWheelFriction", CarFiveWheelFriction);
        }
    }

    //set Car wheel friction
    void SetWheelFriction()
    {
        if (transform.tag == "CarOneWheel")
        {
            wheelCollider.sharedMaterial.friction = CarOneWheelFriction;
        }
        if (transform.tag == "CarTwoWheel")
        {
            wheelCollider.sharedMaterial.friction = CarTwoWheelFriction;
        }
        if (transform.tag == "CarThreeWheel")
        {
            wheelCollider.sharedMaterial.friction = CarThreeWheelFriction;
        }
        if (transform.tag == "CarFourWheel")
        {
            wheelCollider.sharedMaterial.friction = CarFourWheelFriction;
        }
        if (transform.tag == "CarFiveWheel")
        {
            wheelCollider.sharedMaterial.friction = CarFiveWheelFriction;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
