﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserParticle : MonoBehaviour
{
    int i;
    void OnParticleCollision(GameObject other)
    {
        //Debug.Log("on particle collision is called");
        if (other.tag == "Zombie")
        {
            i++;
            if (i == 20)
            {
                if (other.GetComponent<Zombie>())
                {
                    //other.GetComponent<Zombie>().displace = true;
                    Destroy(other);
                }
            }
        }
    }
}
