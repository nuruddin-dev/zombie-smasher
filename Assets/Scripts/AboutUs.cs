﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AboutUs : MonoBehaviour
{
    public bool shouldWait;
    public bool isActive = true;

    // Start is called before the first frame update
    void Start()
    {
        shouldWait = true;
    }

    void FixedUpdate()
    {
        Debug.Log(transform.position.y);
        if (shouldWait)
        {
            StartCoroutine(WaitForNextShow());
        }
        //Debug.Log("y position: " + transform.position.y);
        if (!shouldWait)
        {
            if (transform.position.y < 1904f)
            {
                transform.position = new Vector2(transform.position.x, transform.position.y + 5f);
            }
            else
            {
                StartCoroutine(WaitAtEnd());
            }
        }
    }

    IEnumerator WaitAtEnd()
    {
        yield return new WaitForSeconds(2f);
        transform.position = new Vector2(transform.position.x, -464f);
        shouldWait = true;
    }
    IEnumerator WaitForNextShow()
    {
        Debug.Log("wait for next show is called");
        yield return new WaitForSeconds(2f);
        shouldWait = false;
    }

    public void GoToHome()
    {
        shouldWait = true;
        isActive = true;
        transform.position = new Vector2(transform.position.x, -464f);
        GameObject.Find("MenuManager").GetComponent<MenuManager>().Home();
    }
}
