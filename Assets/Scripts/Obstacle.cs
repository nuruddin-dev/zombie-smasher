﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Car")
        {
            Instantiate(gameObject, new Vector2(transform.position.x + 500, GameObject.Find("LevelManager").transform.position.y), transform.rotation);
            rb.AddForce(Vector2.right*collision.relativeVelocity);
            Destroy(gameObject, 3f);
        }
    }
}
