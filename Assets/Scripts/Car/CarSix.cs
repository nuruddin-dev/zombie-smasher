﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarSix : MonoBehaviour
{
    public float CarSpeed;
    public int minSpeed, maxSpeed;
    public float rotationSpeed = 150f;
    public WheelJoint2D weaponJoint, frontWheel, backWheel;
    public float movement;
    //private float rotation = 0f;
    public Rigidbody2D rb;
    public bool goForward, slowDown;
    public bool weapon;
    public bool finish = false;
    public GameObject GearButton, FireButton;
    private bool isRotatingLeft = false, isRotatingRight = false;
    public bool canFire, shield;
    public ParticleSystem shieldParticle;
    public int health = 100;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("CarSixSpeed"))
        {
            CarSpeed = PlayerPrefs.GetInt("CarSixSpeed");
        }
        else CarSpeed = 1000;
        minSpeed = 0;
        maxSpeed = (int)CarSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Battery.batteryPower > 0)
            canFire = true;
        else canFire = false;

        //Shield activation
        if (shield || Input.GetKey(KeyCode.S))
        {
            Battery.batteryPower--;
            shieldParticle.Play();
        }
        else
        {
            shieldParticle.Stop();
        }

        //Saw activation
        if (weapon || Input.GetKey(KeyCode.Space) && canFire)
        {
            Battery.batteryPower--;
            weaponJoint.useMotor = true;
            Debug.Log("CarSix usemotor: " + weaponJoint.useMotor);
        }
        else
        {
            Debug.Log("CarSix usemotor: " + weaponJoint.useMotor);
            weaponJoint.useMotor = false;
        }


        if (goForward && !finish)
        {
            if (minSpeed < maxSpeed)
            {
                minSpeed += 10;
                movement = -minSpeed;
            }
            else
            {
                movement = -maxSpeed;
                minSpeed = maxSpeed;
            }
            if (transform.rotation.z >= .30f && !isRotatingRight)
            {
                //Debug.Log("rotation is greater then .3");
                StartCoroutine(RotateRight());
            }
            if (transform.rotation.z < -.30f && !isRotatingLeft)
            {
                //Debug.Log("rotation is less then .3");
                StartCoroutine(RotateLeft());
            }
        }

        if (slowDown && !finish)
        {
            if (minSpeed > 0)
            {
                minSpeed -= 10;
                movement = -minSpeed;
            }
            else
            {
                movement = 0f;
                minSpeed = 0;
            }
            if (transform.rotation.z >= .30f && !isRotatingRight)
            {
                //Debug.Log("rotation is greater then .3");
                StartCoroutine(RotateRight());
            }
            if (transform.rotation.z < -.30f && !isRotatingLeft)
            {
                //Debug.Log("rotation is less then .3");
                StartCoroutine(RotateLeft());
            }
        }
    }
    public IEnumerator RotateRight()
    {
        //Debug.Log("RotateRight is called");
        isRotatingRight = true;

        while (transform.rotation.z > .1)
        {
            transform.Rotate(new Vector3(0, 0, -1));
            yield return 0;
        }
        isRotatingRight = false;
    }
    public IEnumerator RotateLeft()
    {
        //Debug.Log("RotateLeft is called");
        isRotatingLeft = true;
        while (transform.rotation.z < .1)
        {
            transform.Rotate(new Vector3(0, 0, 1));
            yield return 0;
        }
        isRotatingLeft = false;
    }
    void FixedUpdate()
    {
        if (movement == 0f)
        {
            frontWheel.useMotor = false;
            backWheel.useMotor = false;
        }
        else
        {
            frontWheel.useMotor = true;
            backWheel.useMotor = true;
            JointMotor2D motor = new JointMotor2D { motorSpeed = movement, maxMotorTorque = 10000 };
            JointMotor2D motorBig = new JointMotor2D { motorSpeed = movement, maxMotorTorque = 10000 };
            frontWheel.motor = motor;
            backWheel.motor = motorBig;
        }
    }

    public void GoForward()
    {
        goForward = true;
        slowDown = false;
    }

    public void SlowDown()
    {
        slowDown = true;
        goForward = false;
    }

    //void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.transform.tag == "Ground")
    //    {
    //        movement = 0.001f;
    //        finish = true;
    //        GearButton = GameObject.Find("Gear Button");
    //        FireButton = GameObject.Find("Fire Button");
    //        GearButton.GetComponent<Button>().interactable = false;
    //        FireButton.GetComponent<Button>().interactable = false;
    //        rb.velocity = Vector2.zero;
    //    }
    //}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Zombie" && !shield)
        {
            health -= 5;
        }
    }
}
