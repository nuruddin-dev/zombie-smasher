﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarOne : MonoBehaviour
{
    public float CarSpeed;
    public int minSpeed, maxSpeed;
    public WheelJoint2D backWheel, frontWheel;
    public float movement;
    public Rigidbody2D rb, missileRB;
    public bool goForward, slowDown;
    public GameObject firePoint, missile, fire, missileFire;
    public bool weapon;
    bool canFire = true;
    public bool finish = false;
    public GameObject GearButton, FireButton;
    private bool isRotatingLeft = false, isRotatingRight = false;
    public bool shield, canShield;
    public ParticleSystem shieldParticle;
    public int health = 100;
    public GameObject ReplayPanel;

    // Start is called before the first frame update
    void Start()
    {
        //if (GameObject.Find("ReplayPanel") != null)
        //{
        //   ReplayPanel = GameObject.Find("ReplayPanel");
        //   ReplayPanel.SetActive(false);
        //}

        if (PlayerPrefs.HasKey("CarOneSpeed"))
        {
            CarSpeed = PlayerPrefs.GetInt("CarOneSpeed");
        }
        else CarSpeed = 1000;
        minSpeed = 0;
        maxSpeed = (int) CarSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("health: " + health);
        if (health <= 0)
        {
            Time.timeScale = 0;
            GameObject.FindWithTag("Ground").GetComponent<LevelFinish>().UIButtons.SetActive(false);
            GameObject.FindWithTag("Ground").GetComponent<LevelFinish>().ReplayPanel.SetActive(true);
        }
        if (Battery.batteryPower > 0)
            canShield = true;
        else canShield = false;

        //Activate shield
        if(shield || Input.GetKey(KeyCode.S) && canShield)
        {
            Battery.batteryPower --;
            shieldParticle.Play();
        }
        else
        {
            shieldParticle.Stop();
        }

        //Fire missile
        if (Input.GetKeyDown(KeyCode.F)||weapon && canFire)
        {
            if (GameObject.FindWithTag("Weapon") == null)
            {
                missile.SetActive(false);
                missileFire = Instantiate(fire, firePoint.transform.position, firePoint.transform.rotation);
                missileRB = missileFire.GetComponent<Rigidbody2D>();
                missileRB.AddForce(transform.right * (rb.velocity.x + 30f), ForceMode2D.Impulse);
                StartCoroutine("RespawnWeapon");
            }
        }
        if (goForward && !finish)
        {
            if (minSpeed < maxSpeed)
            {
                minSpeed += 100;
                movement = -minSpeed;
            }
            else
            {
                movement = -maxSpeed;
                minSpeed = maxSpeed;
            }
            if (transform.rotation.z >= .30f && !isRotatingRight)
            {
                //Debug.Log("rotation is greater then .3");
                StartCoroutine(RotateRight());
            }
            if (transform.rotation.z < -.30f && !isRotatingLeft)
            {
                //Debug.Log("rotation is less then .3");
                StartCoroutine(RotateLeft());
            }
        }

        if (slowDown && !finish)
        {
            if (minSpeed > 0)
            {
                minSpeed -= 50;
                movement = -minSpeed;
            }
            else
            {
                movement = 0;
            }
            if (transform.rotation.z >= .30f && !isRotatingRight)
            {
                //Debug.Log("rotation is greater then .3");
                StartCoroutine(RotateRight());
            }
            if (transform.rotation.z < -.30f && !isRotatingLeft)
            {
                //Debug.Log("rotation is less then .3");
                StartCoroutine(RotateLeft());
            }
        }
    }


    public IEnumerator RotateRight()
    {
        //Debug.Log("RotateRight is called");
        isRotatingRight = true;

        while (transform.rotation.z > .1)
        {
            transform.Rotate(new Vector3(0, 0, -1));
            yield return 0;
        }
        isRotatingRight = false;
    }
    public IEnumerator RotateLeft()
    {
        //Debug.Log("RotateLeft is called");
        isRotatingLeft = true;
        while (transform.rotation.z <.1)
        {
            transform.Rotate(new Vector3(0, 0, 1));
            yield return 0;
        }
        isRotatingLeft = false;
    }

    IEnumerator RespawnWeapon()
    {
        canFire = false;
        yield return new WaitForSeconds(2f);
        Destroy(missileFire);
        missile.SetActive(true);
        canFire = true;
    }
    void FixedUpdate()
    {
        if (movement == 0f)
        {
            backWheel.useMotor = false;
            frontWheel.useMotor = false;
        }
        else
        {
            backWheel.useMotor = true;
            frontWheel.useMotor = true;
            JointMotor2D motor = new JointMotor2D { motorSpeed = movement, maxMotorTorque = 10000 };
            backWheel.motor = motor;
            frontWheel.motor = motor;
        }
    }

    public void GoForward()
    {
        goForward = true;
        slowDown = false;
    }

    public void SlowDown()
    {
        slowDown = true;
        goForward = false;
    }

    //void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.transform.tag == "Ground")
    //    {
    //        movement = 0.001f;
    //        finish = true;
    //        GearButton = GameObject.Find("Gear Button");
    //        FireButton = GameObject.Find("Fire Button");
    //        GearButton.GetComponent<Button>().interactable = false;
    //        FireButton.GetComponent<Button>().interactable = false;
    //        rb.velocity = Vector2.zero;
    //    }
    //}
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Zombie" && !shield)
        {
            health -=5;
        }
    }
}
