﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarFive : MonoBehaviour
{
    public float CarSpeed;
    public int minSpeed, maxSpeed;
    public float rotationSpeed = 150f;
    public WheelJoint2D bigWheel, smallWheelFront, smallWheelBack;
    public float movement;
    private float rotation = 0f;
    public Rigidbody2D rb;
    public bool goForward, slowDown;
    public bool weapon, shield;
    public ParticleSystem laserParticle, shieldParticle;
    public bool finish = false;
    public GameObject GearButton, FireButton;
    private bool isRotatingLeft = false, isRotatingRight = false;
    public bool canFire;
    public int health = 100;

    // Start is called before the first frame update
    void Start()
    {
        //laserParticle = GetComponentInChildren<ParticleSystem>();
        if (PlayerPrefs.HasKey("CarFiveSpeed"))
        {
            CarSpeed = PlayerPrefs.GetInt("CarFiveSpeed");
        }
        else CarSpeed = 2000;
        minSpeed = 0;
        maxSpeed = (int)CarSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Battery.batteryPower > 0)
            canFire = true;
        else canFire = false;

        //Activate laser
        if (weapon || Input.GetKey(KeyCode.Space) && canFire)
        {
            Battery.batteryPower--;
            laserParticle.Play();
        }
        else
        {
            laserParticle.Stop();
        }

        //Activate shield
        if (shield || Input.GetKey(KeyCode.S) && canFire)
        {
            Battery.batteryPower--;
            shieldParticle.Play();
        }
        else
        {
            shieldParticle.Stop();
        }


        //Debug.Log("movement: " + movement);
        if (goForward && !finish)
        {
            if (minSpeed < maxSpeed)
            {
                minSpeed += 100;
                movement = -minSpeed;
            }
            else
            {
                movement = -maxSpeed;
                minSpeed = maxSpeed;
            }
            if (transform.rotation.z >= .30f && !isRotatingRight)
            {
                //Debug.Log("rotation is greater then .3");
                StartCoroutine(RotateRight());
            }
            if (transform.rotation.z < -.30f && !isRotatingLeft)
            {
                //Debug.Log("rotation is less then .3");
                StartCoroutine(RotateLeft());
            }
        }

        if (slowDown && !finish)
        {
            if (minSpeed > 0)
            {
                minSpeed -= 50;
                movement = -minSpeed;
            }
            else
            {
                movement = 0f;
                minSpeed = 0;
            }
            if (transform.rotation.z >= .30f && !isRotatingRight)
            {
                //Debug.Log("rotation is greater then .3");
                StartCoroutine(RotateRight());
            }
            if (transform.rotation.z < -.30f && !isRotatingLeft)
            {
                //Debug.Log("rotation is less then .3");
                StartCoroutine(RotateLeft());
            }
        }
        rotation = -Input.GetAxisRaw("Horizontal");
    }
    public IEnumerator RotateRight()
    {
        //Debug.Log("RotateRight is called");
        isRotatingRight = true;

        while (transform.rotation.z > .1)
        {
            transform.Rotate(new Vector3(0, 0, -1));
            yield return 0;
        }
        isRotatingRight = false;
    }
    public IEnumerator RotateLeft()
    {
        //Debug.Log("RotateLeft is called");
        isRotatingLeft = true;
        while (transform.rotation.z < .1)
        {
            transform.Rotate(new Vector3(0, 0, 1));
            yield return 0;
        }
        isRotatingLeft = false;
    }
    void FixedUpdate()
    {
        if (movement == 0f)
        {
            bigWheel.useMotor = false;
            smallWheelFront.useMotor = false;
            smallWheelBack.useMotor = false;
        }
        else
        {
            bigWheel.useMotor = true;
            smallWheelFront.useMotor = true;
            smallWheelBack.useMotor = true;
            JointMotor2D motor = new JointMotor2D { motorSpeed = movement, maxMotorTorque = 10000 };
            bigWheel.motor = motor;
            smallWheelFront.motor = motor;
            smallWheelBack.motor = motor;
        }

        //rb.AddTorque(-rotation * rotationSpeed * Time.deltaTime);
    }

    public void GoForward()
    {
        goForward = true;
        slowDown = false;
    }

    public void SlowDown()
    {
        slowDown = true;
        goForward = false;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Zombie" && !shield)
        {
            health -= 5;
        }
    }
}
