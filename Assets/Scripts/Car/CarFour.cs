﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarFour : MonoBehaviour
{
    public float CarSpeed;
    public int minSpeed, maxSpeed;
    public float rotationSpeed = 150f;
    public WheelJoint2D backWheel, frontWheel;
    public float movement;
    private float rotation = 0f;
    public Rigidbody2D rb;
    public bool goForward, slowDown;
    public bool weapon, shield, canFire, canShield;
    public Animator carAnimation; 
    public bool finish = false;
    public GameObject GearButton, FireButton;
    private bool isRotatingLeft = false, isRotatingRight = false;
    public ParticleSystem shieldParticle;
    public int health = 100;

    // Start is called before the first frame update
    void Start()
    {
        carAnimation = GetComponent<Animator>();

        if (PlayerPrefs.HasKey("CarFourSpeed"))
        {
            CarSpeed = PlayerPrefs.GetInt("CarFourSpeed");
        }
        else CarSpeed = 300;
        minSpeed = 0;
        maxSpeed = (int)CarSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        carAnimation.SetBool("weapon", weapon);
        
        if (Battery.batteryPower > 0)
            canShield = true;
        else canShield = false;


        if (shield || Input.GetKey(KeyCode.S) && canShield)
        {
            shieldParticle.Play();
            Battery.batteryPower--;
        }
        else
        {
            shieldParticle.Stop();
        }

        
        //Debug.Log(weapon);
        carAnimation.SetBool("weapon",weapon);
        Debug.Log("movement: " + movement);
        if (goForward && !finish)
        {
            if (minSpeed < maxSpeed)
            {
                minSpeed += 100;
                movement = -minSpeed;
            }
            else
            {
                movement = -maxSpeed;
                minSpeed = maxSpeed;
            }
            if (transform.rotation.z >= .30f && !isRotatingRight)
            {
                //Debug.Log("rotation is greater then .3");
                StartCoroutine(RotateRight());
            }
            if (transform.rotation.z < -.30f && !isRotatingLeft)
            {
                //Debug.Log("rotation is less then .3");
                StartCoroutine(RotateLeft());
            }
        }

        if (slowDown && !finish)
        {
            if (minSpeed > 0)
            {
                minSpeed -= 50;
                movement = -minSpeed;
            }
            else
            {
                movement = 0f;
                minSpeed = 0;
            }
            if (transform.rotation.z >= .30f && !isRotatingRight)
            {
                //Debug.Log("rotation is greater then .3");
                StartCoroutine(RotateRight());
            }
            if (transform.rotation.z < -.30f && !isRotatingLeft)
            {
                //Debug.Log("rotation is less then .3");
                StartCoroutine(RotateLeft());
            }
        }
        rotation = -Input.GetAxisRaw("Horizontal");
    }
    public IEnumerator RotateRight()
    {
        //Debug.Log("RotateRight is called");
        isRotatingRight = true;

        while (transform.rotation.z > .1)
        {
            transform.Rotate(new Vector3(0, 0, -1));
            yield return 0;
        }
        isRotatingRight = false;
    }
    public IEnumerator RotateLeft()
    {
        //Debug.Log("RotateLeft is called");
        isRotatingLeft = true;
        while (transform.rotation.z < .1)
        {
            transform.Rotate(new Vector3(0, 0, 1));
            yield return 0;
        }
        isRotatingLeft = false;
    }
    void FixedUpdate()
    {
        if (movement == 0f)
        {
            backWheel.useMotor = false;
            frontWheel.useMotor = false;
        }
        else
        {
            backWheel.useMotor = true;
            frontWheel.useMotor = true;
            JointMotor2D motor = new JointMotor2D { motorSpeed = movement, maxMotorTorque = 10000 };
            JointMotor2D motorBig = new JointMotor2D { motorSpeed = movement, maxMotorTorque = 10000 };
            backWheel.motor = motorBig;
            frontWheel.motor = motor;
        }

        //rb.AddTorque(-rotation * rotationSpeed * Time.deltaTime);
    }

    public void GoForward()
    {
        goForward = true;
        slowDown = false;
    }

    public void SlowDown()
    {
        slowDown = true;
        goForward = false;
    }

    //void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.transform.tag == "Ground")
    //    {
    //        movement = 0.001f;
    //        finish = true;
    //        GearButton = GameObject.Find("Gear Button");
    //        FireButton = GameObject.Find("Fire Button");
    //        GearButton.GetComponent<Button>().interactable = false;
    //        FireButton.GetComponent<Button>().interactable = false;
    //        rb.velocity = Vector2.zero;
    //    }
    //}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Zombie" && !shield)
        {
            health -= 5;
        }
    }
}
