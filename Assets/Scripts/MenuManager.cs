﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public GameObject  AllCar, Garage, StartPanel, LevelSelectPanel, MenuPanel,
                      UpgradePanel, AboutUsPanel, CoinText, CoinIcon;
    public GameObject SoundOff, MusicOff;
    private int menuFromLevel;
    public AudioSource menuAudio;
    //public AudioClip backgroundAudio, buttonClickAudio;
    int musicStatus, soundStatus;

    int i;

    

    void Awake()
    {
        //It determines whether the scene open start_panel or garage_panel when the scene is called
        menuFromLevel = PlayerPrefs.GetInt("MenuFromLevel", 0); 

        //Prevent screen to time out while game is playing
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        
        //Music status checking
        if (PlayerPrefs.HasKey("MusicStatus"))
        {
            musicStatus = PlayerPrefs.GetInt("MusicStatus");
        }
        else
        {
            musicStatus = 1;
            PlayerPrefs.SetInt("MusicStatus", 1);
        }

        //Sound Status checking
        if (PlayerPrefs.HasKey("SoundStatus"))
        {
            soundStatus = PlayerPrefs.GetInt("SoundStatus");
        }
        else
        {
            soundStatus = 1;
            PlayerPrefs.SetInt("SoundStatus", 1);
        }

        Garage = GameObject.Find("Garage");
        AllCar = GameObject.Find("AllCar");
        StartPanel = GameObject.Find("StartPanel");
        LevelSelectPanel = GameObject.Find("LevelSelectPanel");
        MenuPanel = GameObject.Find("MenuPanel");
        UpgradePanel = GameObject.Find("UpgradePanel");
        AboutUsPanel = GameObject.Find("AboutUsPanel");
        CoinText = GameObject.Find("CoinText");
        CoinIcon = GameObject.Find("CoinIcon");
        MusicOff = GameObject.Find("MusicOff");
        SoundOff = GameObject.Find("SoundOff");
    }
    // Start is called before the first frame update
    void Start()
    {
        //menuFromLevel = PlayerPrefs.GetInt("MenuFromLevel", 0);

        menuAudio = GetComponent<AudioSource>();

        if (musicStatus == 1)
        {
            MusicOff.SetActive(false);
            FindObjectOfType<AudioManager>().MusicPlay("Menu_Background");
            //menuAudio.Play();
            //AudioPlay(backgroundMusic);
        }
        else
        {
            MusicOff.SetActive(true);
            FindObjectOfType<AudioManager>().MusicStop("Menu_Background");
            //menuAudio.Stop();
            //AudioStop(backgroundMusic);
        }

        if (soundStatus == 1)
        {
            SoundOff.SetActive(false);
        }
        else
        {
            SoundOff.SetActive(true);
        }

        if (menuFromLevel == 0)
        {
            Garage.SetActive(false);
            AllCar.SetActive(false);
            StartPanel.SetActive(true);
            LevelSelectPanel.SetActive(false);
            MenuPanel.SetActive(false);
            UpgradePanel.SetActive(false);
            AboutUsPanel.SetActive(false);
            CoinText.SetActive(false);
            CoinIcon.SetActive(false);
        }
        else
        {
            PlayerPrefs.SetInt("MenuFromLevel", 0);
            Garage.SetActive(true);
            AllCar.SetActive(true);
            StartPanel.SetActive(false);
            LevelSelectPanel.SetActive(false);
            MenuPanel.SetActive(true);
            UpgradePanel.SetActive(false);
            AboutUsPanel.SetActive(false);
            CoinText.SetActive(true);
            CoinIcon.SetActive(true);
        }
    }

    //Music Button
    public void MusicButton()
    {
        Debug.Log("music button is called");
        musicStatus = PlayerPrefs.GetInt("MusicStatus");
        Debug.Log("music status: " + musicStatus);
        if (musicStatus == 1)
        {
            MusicOff.SetActive(true);
            //save status
            PlayerPrefs.SetInt("MusicStatus", 0);
            FindObjectOfType<AudioManager>().MusicStop("Menu_Background");
            //AudioStop(backgroundMusic);
            //menuAudio.Stop();
        }
        else
        {
            MusicOff.SetActive(false);
            //save status
            PlayerPrefs.SetInt("MusicStatus", 1);
            FindObjectOfType<AudioManager>().MusicPlay("Menu_Background");
            //AudioPlay(backgroundMusic);
            //menuAudio.Play();
        }
    }

    //Sound Button
    public void SoundButton()
    {
        Debug.Log("sound button is called");
        soundStatus = PlayerPrefs.GetInt("SoundStatus");
        Debug.Log("sound status: " + soundStatus);
        if (soundStatus == 1)
        {
            SoundOff.SetActive(true);
            //save status
            PlayerPrefs.SetInt("SoundStatus", 0);
        }
        else
        {
            SoundOff.SetActive(false);
            //save status
            PlayerPrefs.SetInt("SoundStatus", 1);
        }
    }

    void Update()
    {
        //Debug.Log("coin: " + PlayerPrefs.GetFloat("Coin"));
        if (CoinText != null)
        {
            CoinText.GetComponent<Text>().text = PlayerPrefs.GetFloat("Coin").ToString();
        }
        soundStatus = PlayerPrefs.GetInt("SoundStatus");
    }
    public void Home()
    {
        //Button clicked audio
        if(soundStatus == 1)
        FindObjectOfType<AudioManager>().Play("Button_Clicked");

        //Garage.SetActive(false);
        AllCar.SetActive(false);
        StartPanel.SetActive(true);
        LevelSelectPanel.SetActive(false);
        MenuPanel.SetActive(false);
        UpgradePanel.SetActive(false);
        AboutUsPanel.SetActive(false);
        CoinText.SetActive(false);
        CoinIcon.SetActive(false);
    }

    public void Play()
    {
        //Button clicked audio
        if (soundStatus == 1)
        FindObjectOfType<AudioManager>().Play("Button_Clicked");
        Debug.Log("Play button clicked " + soundStatus);

        Garage.SetActive(false);
        AllCar.SetActive(false);
        StartPanel.SetActive(false);
        LevelSelectPanel.SetActive(true);
        MenuPanel.SetActive(false);
        UpgradePanel.SetActive(false);
        AboutUsPanel.SetActive(false);
        CoinText.SetActive(false);
        CoinIcon.SetActive(false);
    }

    public void Exit()
    {
        //Button clicked audio
        if (soundStatus == 1)
        FindObjectOfType<AudioManager>().Play("Button_Clicked");

        Application.Quit();
    }

    public void Menu()
    {
        //Button clicked audio
        if (soundStatus == 1)
        FindObjectOfType<AudioManager>().Play("Button_Clicked");

        StartPanel.SetActive(false);
        AllCar.SetActive(true);
        MenuPanel.SetActive(true);
        Garage.SetActive(true);
        CoinText.SetActive(true);
        CoinIcon.SetActive(true);
        UpgradePanel.SetActive(false);
        LevelSelectPanel.SetActive(false);
        AboutUsPanel.SetActive(false);
    }

    public void AboutUs()
    {
        //Button clicked audio
        if (soundStatus == 1)
        FindObjectOfType<AudioManager>().Play("Button_Clicked");

        StartPanel.SetActive(false);
        AllCar.SetActive(false);
        MenuPanel.SetActive(false);
        Garage.SetActive(false);
        CoinText.SetActive(false);
        CoinIcon.SetActive(false);
        UpgradePanel.SetActive(false);
        LevelSelectPanel.SetActive(false);
        AboutUsPanel.SetActive(true);
    }

    public void CloseAbout()
    {
        i++;
        PlayerPrefs.SetInt("AboutUs", i);

        //Button clicked audio
        if (soundStatus == 1)
        FindObjectOfType<AudioManager>().Play("Button_Clicked");

        StartPanel.SetActive(false);
        AllCar.SetActive(true);
        MenuPanel.SetActive(true);
        Garage.SetActive(true);
        CoinText.SetActive(true);
        CoinIcon.SetActive(true);
        UpgradePanel.SetActive(false);
        LevelSelectPanel.SetActive(false);
        AboutUsPanel.SetActive(false);
    }
}
