﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CarManager : BaseClass
{
    static public int selectedCar = 0;
    public GameObject OkButton, UpgradeButton;
    public GameObject Lock, CarPrice;

    // Start is called before the first frame update
    void Start()
    {
        SelectCar();
        if(GameObject.Find("Ok")!= null)
            OkButton = GameObject.Find("Ok");
        if (GameObject.Find("UpgradeButton") != null)
            UpgradeButton = GameObject.Find("UpgradeButton");
        if (GameObject.Find("UnlockButton") != null)
            Lock = GameObject.Find("UnlockButton");
        //if (GameObject.Find("CarPrice") != null)
            //CarPrice = GameObject.Find("CarPrice");
    }

    // Update is called once per frame
    void Update()
    {
        //PlayerPrefs.SetFloat("Coin", coin);
        if (selectedCar == 0)
        {
            Unlocked();
        }
        else
        {
            if (selectedCar == 1)
            {
                if (PlayerPrefs.GetInt("CarTwoUnlock") == 1)
                    Unlocked();
                else
                    Locked();
            }
            if (selectedCar == 2)
            {
                if (PlayerPrefs.GetInt("CarThreeUnlock") == 1)
                    Unlocked();
                else
                    Locked();
            }
            if (selectedCar == 3)
            {
                if (PlayerPrefs.GetInt("CarFourUnlock") == 1)
                    Unlocked();
                else
                    Locked();
            }
            if (selectedCar == 4)
            {
                if (PlayerPrefs.GetInt("CarFiveUnlock") == 1)
                    Unlocked();
                else
                    Locked();
            }
            if (selectedCar == 5)
            {
                if (PlayerPrefs.GetInt("CarSixUnlock") == 1)
                    Unlocked();
                else
                    Locked();
            }
            if (selectedCar == 6)
            {
                if (PlayerPrefs.GetInt("CarSevenUnlock") == 1)
                    Unlocked();
                else
                    Locked();
            }
        }
        
    }

    void Locked()
    {
        OkButton.GetComponent<Button>().interactable = false;
        UpgradeButton.GetComponent<Button>().interactable = false;
        Lock.SetActive(true);
        //CarPrice.SetActive(true);
    }

    void Unlocked()
    {
        OkButton.GetComponent<Button>().interactable = true;
        UpgradeButton.GetComponent<Button>().interactable = true;
        Lock.SetActive(false);
        //CarPrice.SetActive(false);
    }

    public void NextCar()
    {
        //Button clicked audio
        FindObjectOfType<AudioManager>().Play("Button_Clicked");

        int previousSelectedCar = selectedCar;
        if (selectedCar >= transform.childCount - 1)
        {
            selectedCar = 0;
        }
        else selectedCar++;
        if (previousSelectedCar != selectedCar)
        {
            SelectCar();
        }

        Debug.Log("Car no " + (selectedCar+1) + " is selected");
    }

    public void PreviousCar()
    {
        //Button clicked audio
        FindObjectOfType<AudioManager>().Play("Button_Clicked");

        int previousSelectedCar = selectedCar;
        if (selectedCar <= 0)
        {
            selectedCar = transform.childCount - 1;
        }
        else selectedCar--;
        if (previousSelectedCar != selectedCar)
        {
            SelectCar();
        }
        Debug.Log("Car no " + (selectedCar + 1) + " is selected");
    }

    void SelectCar()
    {
        int i = 0;

        foreach (Transform Car in transform)
        {
            if (i == selectedCar)
            {
                Car.gameObject.SetActive(true);
            }
            else Car.gameObject.SetActive(false);
            i++;
        }
    }

    //Go to next scene
    public void Go()
    {
        SceneManager.LoadScene("Game");
    }

    public void Unlock()
    {
        float coin = PlayerPrefs.GetFloat("Coin");
        if (coin >= 5000f)
        {
            if (selectedCar == 1)
            {
                PlayerPrefs.SetInt("CarTwoUnlock", 1);
                coin = coin - 5000f;
                PlayerPrefs.SetFloat("Coin", coin);
            }
            if (selectedCar == 2)
            {
                PlayerPrefs.SetInt("CarThreeUnlock", 1);
                coin = coin - 5000f;
                PlayerPrefs.SetFloat("Coin", coin);
            }
            if (selectedCar == 3)
            {
                PlayerPrefs.SetInt("CarFourUnlock", 1);
                coin = coin - 5000f;
                PlayerPrefs.SetFloat("Coin", coin);
            }
            if (selectedCar == 4)
            {
                PlayerPrefs.SetInt("CarFiveUnlock", 1);
                coin = coin - 5000f;
                PlayerPrefs.SetFloat("Coin", coin);
            }
            if (selectedCar == 5)
            {
                PlayerPrefs.SetInt("CarSixUnlock", 1);
                coin = coin - 5000f;
                PlayerPrefs.SetFloat("Coin", coin);
            }
            if (selectedCar == 6)
            {
                PlayerPrefs.SetInt("CarSevenUnlock", 1);
                coin = coin - 5000f;
                PlayerPrefs.SetFloat("Coin", coin);
            }
        }
    }
}
