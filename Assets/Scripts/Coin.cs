﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coin : MonoBehaviour
{
    public float coin;
    public Text CoinField;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        coin = PlayerPrefs.GetFloat("Coin");
        CoinField.text = coin.ToString();
    }
}
