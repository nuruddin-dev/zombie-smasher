﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieEgg : MonoBehaviour
{
    public Rigidbody2D EggRigidbody;
    public GameObject myCar,Bomb;
    GameObject partical;
    


    // Start is called before the first frame update
    void Start()
    {
        EggRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        myCar = GameObject.FindGameObjectWithTag("Car");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Car" || collision.gameObject.tag == "Ground")
        {

            partical = Instantiate(Bomb, transform.position, transform.rotation);
            Destroy(partical, 1f);
        }
        Destroy(gameObject, 1f);
    }
    //IEnumerator DestroyDellayEgg()
    //{
    //    yield return new WaitForSeconds(.01f);
    //}
}
