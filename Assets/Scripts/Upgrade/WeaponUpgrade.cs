﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponUpgrade : CarManager
{
    new int selectedCar;
    float coin;
    public Text WeaponValue, WeaponLevel;
    public GameObject WeaponCoinIcon;
    int CarOneWeaponUpdatePercentage, CarTwoWeaponUpdatePercentage, CarThreeWeaponUpdatePercentage, CarFourWeaponUpdatePercentage, CarFiveWeaponUpdatePercentage, CarSixWeaponUpdatePercentage, CarSevenWeaponUpdatePercentage;
    float CarOneWeaponCurrentValue, CarTwoWeaponCurrentValue, CarThreeWeaponCurrentValue, CarFourWeaponCurrentValue, CarFiveWeaponCurrentValue, CarSixWeaponCurrentValue, CarSevenWeaponCurrentValue;
    float CarOneCoinNeedForWeapon, CarTwoCoinNeedForWeapon, CarThreeCoinNeedForWeapon, CarFourCoinNeedForWeapon, CarFiveCoinNeedForWeapon, CarSixCoinNeedForWeapon, CarSevenCoinNeedForWeapon;
    int CarOneWeaponLevel, CarTwoWeaponLevel, CarThreeWeaponLevel, CarFourWeaponLevel, CarFiveWeaponLevel, CarSixWeaponLevel, CarSevenWeaponLevel;
    float CarOneWeaponDamage, CarTwoWeaponDamage, CarThreeWeaponDamage, CarFourWeaponDamage, CarFiveWeaponDamage, CarSixWeaponDamage, CarSevenWeaponDamage;

    void Awake()
    {
        WeaponCoinIcon = GameObject.Find("WeaponCoinIcon");
        WeaponValue = GameObject.Find("WeaponValue").GetComponent<Text>();
        WeaponLevel = GameObject.Find("WeaponLevel").GetComponent<Text>();
    }
    //Upgrade All Car Weapon
    public void UpgradeWeapon()
    {
        //selectedCar = CarManager.selectedCar;
        //selectedCar = GameObject.Find("CarManager").GetComponent<CarManager>().selectedCar;
        selectedCar = CarManager.selectedCar;

        coin = PlayerPrefs.GetFloat("Coin");
        CarOneWeaponLevel = PlayerPrefs.GetInt("CarOneWeaponLevel");
        CarTwoWeaponLevel = PlayerPrefs.GetInt("CarTwoWeaponLevel");
        CarThreeWeaponLevel = PlayerPrefs.GetInt("CarThreeWeaponLevel");
        CarFourWeaponLevel = PlayerPrefs.GetInt("CarFourWeaponLevel");
        CarFiveWeaponLevel = PlayerPrefs.GetInt("CarFiveWeaponLevel");
        CarSixWeaponLevel = PlayerPrefs.GetInt("CarSixWeaponLevel");
        CarSevenWeaponLevel = PlayerPrefs.GetInt("CarSevenWeaponLevel");


        int WeaponUpdateValue = 100;

        if (selectedCar == 0)
        {
            Debug.Log("Car one Weapon level : " + CarOneWeaponLevel);
            if (CarOneWeaponLevel < 10)
            {
                CarOneCoinNeedForWeapon = PlayerPrefs.GetFloat("CarOneCoinNeedForWeapon");
                CarOneWeaponUpdatePercentage = CarOneWeaponLevel + 10;
                if (coin >= CarOneCoinNeedForWeapon)
                {
                    coin = coin - CarOneCoinNeedForWeapon;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarOneCoinNeedForWeapon = CoinNeed(CarOneCoinNeedForWeapon, CarOneWeaponUpdatePercentage);
                    PlayerPrefs.SetFloat("CarOneCoinNeedForWeapon", CarOneCoinNeedForWeapon);
                    if (CarOneWeaponLevel == 9)
                    {
                        WeaponValue.text = "Max";
                        WeaponCoinIcon.SetActive(false);
                    }
                    else
                    {
                        WeaponCoinIcon.SetActive(true);
                        WeaponValue.text = PlayerPrefs.GetFloat("CarOneCoinNeedForWeapon").ToString();
                    }
                    CarOneWeaponLevel++;
                    PlayerPrefs.SetInt("CarOneWeaponLevel", CarOneWeaponLevel);
                    WeaponLevel.text = PlayerPrefs.GetInt("CarOneWeaponLevel").ToString() + "/10";
                    CarOneWeaponDamage = CarOneWeaponDamage + WeaponUpdateValue;
                    PlayerPrefs.SetFloat("CarOneWeaponDamage", CarOneWeaponDamage);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarOneWeaponCurrentValue: " + CarOneCoinNeedForWeapon);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarOneCoinNeedForWeapon + "coin");
                }
            }
        }
        if (selectedCar == 1)
        {
            Debug.Log("Car Two Weapon level : " + CarTwoWeaponLevel);
            if (CarTwoWeaponLevel < 10)
            {
                CarTwoCoinNeedForWeapon = PlayerPrefs.GetFloat("CarTwoCoinNeedForWeapon");
                CarTwoWeaponUpdatePercentage = CarTwoWeaponLevel + 10;
                if (coin >= CarTwoCoinNeedForWeapon)
                {
                    coin = coin - CarTwoCoinNeedForWeapon;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarTwoCoinNeedForWeapon = CoinNeed(CarTwoCoinNeedForWeapon, CarTwoWeaponUpdatePercentage);
                    PlayerPrefs.SetFloat("CarTwoCoinNeedForWeapon", CarTwoCoinNeedForWeapon);
                    if (CarTwoWeaponLevel == 9)
                    {
                        WeaponValue.text = "Max";
                        WeaponCoinIcon.SetActive(false);
                    }
                    else
                    {
                        WeaponCoinIcon.SetActive(true);
                        WeaponValue.text = PlayerPrefs.GetFloat("CarTwoCoinNeedForWeapon").ToString();
                    }
                    CarTwoWeaponLevel++;
                    PlayerPrefs.SetInt("CarTwoWeaponLevel", CarTwoWeaponLevel);
                    WeaponLevel.text = PlayerPrefs.GetInt("CarTwoWeaponLevel").ToString() + "/10";
                    CarTwoWeaponDamage = CarTwoWeaponDamage + WeaponUpdateValue;
                    PlayerPrefs.SetFloat("CarTwoWeaponDamage", CarTwoWeaponDamage);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarTwoWeaponCurrentValue: " + CarTwoCoinNeedForWeapon);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarTwoCoinNeedForWeapon + "coin");
                }
            }
        }
        if (selectedCar == 2)
        {
            Debug.Log("Car Three Weapon level : " + CarThreeWeaponLevel);
            if (CarThreeWeaponLevel < 10)
            {
                CarThreeCoinNeedForWeapon = PlayerPrefs.GetFloat("CarThreeCoinNeedForWeapon");
                CarThreeWeaponUpdatePercentage = CarThreeWeaponLevel + 10;
                if (coin >= CarThreeCoinNeedForWeapon)
                {
                    coin = coin - CarThreeCoinNeedForWeapon;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarThreeCoinNeedForWeapon = CoinNeed(CarThreeCoinNeedForWeapon, CarThreeWeaponUpdatePercentage);
                    PlayerPrefs.SetFloat("CarThreeCoinNeedForWeapon", CarThreeCoinNeedForWeapon);
                    if (CarThreeWeaponLevel == 9)
                    {
                        WeaponValue.text = "Max";
                        WeaponCoinIcon.SetActive(false);
                    }
                    else
                    {
                        WeaponCoinIcon.SetActive(true);
                        WeaponValue.text = PlayerPrefs.GetFloat("CarThreeCoinNeedForWeapon").ToString();
                    }
                    CarThreeWeaponLevel++;
                    PlayerPrefs.SetInt("CarThreeWeaponLevel", CarThreeWeaponLevel);
                    WeaponLevel.text = PlayerPrefs.GetInt("CarThreeWeaponLevel").ToString() + "/10";
                    CarThreeWeaponDamage = CarThreeWeaponDamage + WeaponUpdateValue;
                    PlayerPrefs.SetFloat("CarThreeWeaponDamage", CarThreeWeaponDamage);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarThreeWeaponCurrentValue: " + CarThreeCoinNeedForWeapon);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarThreeCoinNeedForWeapon + "coin");
                }
            }
        }
        if (selectedCar == 3)
        {
            Debug.Log("Car Four Weapon level : " + CarFourWeaponLevel);
            if (CarFourWeaponLevel < 10)
            {
                CarFourCoinNeedForWeapon = PlayerPrefs.GetFloat("CarFourCoinNeedForWeapon");
                CarFourWeaponUpdatePercentage = CarFourWeaponLevel + 10;
                if (coin >= CarFourCoinNeedForWeapon)
                {
                    coin = coin - CarFourCoinNeedForWeapon;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarFourCoinNeedForWeapon = CoinNeed(CarFourCoinNeedForWeapon, CarFourWeaponUpdatePercentage);
                    PlayerPrefs.SetFloat("CarFourCoinNeedForWeapon", CarFourCoinNeedForWeapon);
                    if (CarFourWeaponLevel == 9)
                    {
                        WeaponValue.text = "Max";
                        WeaponCoinIcon.SetActive(false);
                    }
                    else
                    {
                        WeaponCoinIcon.SetActive(true);
                        WeaponValue.text = PlayerPrefs.GetFloat("CarFourCoinNeedForWeapon").ToString();
                    }
                    CarFourWeaponLevel++;
                    PlayerPrefs.SetInt("CarFourWeaponLevel", CarFourWeaponLevel);
                    WeaponLevel.text = PlayerPrefs.GetInt("CarFourWeaponLevel").ToString() + "/10";
                    CarFourWeaponDamage = CarFourWeaponDamage + WeaponUpdateValue;
                    PlayerPrefs.SetFloat("CarFourWeaponDamage", CarFourWeaponDamage);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarFourWeaponCurrentValue: " + CarFourCoinNeedForWeapon);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarFourCoinNeedForWeapon + "coin");
                }
            }
        }
        if (selectedCar == 4)
        {
            Debug.Log("Car Five Weapon level : " + CarFiveWeaponLevel);
            if (CarFiveWeaponLevel < 10)
            {
                CarFiveCoinNeedForWeapon = PlayerPrefs.GetFloat("CarFiveCoinNeedForWeapon");
                CarFiveWeaponUpdatePercentage = CarFiveWeaponLevel + 10;
                if (coin >= CarFiveCoinNeedForWeapon)
                {
                    coin = coin - CarFiveCoinNeedForWeapon;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarFiveCoinNeedForWeapon = CoinNeed(CarFiveCoinNeedForWeapon, CarFiveWeaponUpdatePercentage);
                    PlayerPrefs.SetFloat("CarFiveCoinNeedForWeapon", CarFiveCoinNeedForWeapon);
                    if (CarFiveWeaponLevel == 9)
                    {
                        WeaponValue.text = "Max";
                        WeaponCoinIcon.SetActive(false);
                    }
                    else
                    {
                        WeaponCoinIcon.SetActive(true);
                        WeaponValue.text = PlayerPrefs.GetFloat("CarFiveCoinNeedForWeapon").ToString();
                    }
                    CarFiveWeaponLevel++;
                    PlayerPrefs.SetInt("CarFiveWeaponLevel", CarFiveWeaponLevel);
                    WeaponLevel.text = PlayerPrefs.GetInt("CarFiveWeaponLevel").ToString() + "/10";
                    CarFiveWeaponDamage = CarFiveWeaponDamage + WeaponUpdateValue;
                    PlayerPrefs.SetFloat("CarFiveWeaponDamage", CarFiveWeaponDamage);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarFiveWeaponCurrentValue: " + CarFiveCoinNeedForWeapon);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarFiveCoinNeedForWeapon + "coin");
                }
            }
        }
        if (selectedCar == 5)
        {
            Debug.Log("Car Six Weapon level : " + CarSixWeaponLevel);
            if (CarSixWeaponLevel < 10)
            {
                CarSixCoinNeedForWeapon = PlayerPrefs.GetFloat("CarSixCoinNeedForWeapon");
                CarSixWeaponUpdatePercentage = CarSixWeaponLevel + 10;
                if (coin >= CarSixCoinNeedForWeapon)
                {
                    coin = coin - CarSixCoinNeedForWeapon;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarSixCoinNeedForWeapon = CoinNeed(CarSixCoinNeedForWeapon, CarSixWeaponUpdatePercentage);
                    PlayerPrefs.SetFloat("CarSixCoinNeedForWeapon", CarSixCoinNeedForWeapon);
                    if (CarSixWeaponLevel == 9)
                    {
                        WeaponValue.text = "Max";
                        WeaponCoinIcon.SetActive(false);
                    }
                    else
                    {
                        WeaponCoinIcon.SetActive(true);
                        WeaponValue.text = PlayerPrefs.GetFloat("CarSixCoinNeedForWeapon").ToString();
                    }
                    CarSixWeaponLevel++;
                    PlayerPrefs.SetInt("CarSixWeaponLevel", CarSixWeaponLevel);
                    WeaponLevel.text = PlayerPrefs.GetInt("CarSixWeaponLevel").ToString() + "/10";
                    CarSixWeaponDamage = CarSixWeaponDamage + WeaponUpdateValue;
                    PlayerPrefs.SetFloat("CarSixWeaponDamage", CarSixWeaponDamage);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarSixWeaponCurrentValue: " + CarSixCoinNeedForWeapon);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarSixCoinNeedForWeapon + "coin");
                }
            }
        }
        if (selectedCar == 6)
        {
            Debug.Log("Car Seven Weapon level : " + CarSevenWeaponLevel);
            if (CarSevenWeaponLevel < 10)
            {
                CarSevenCoinNeedForWeapon = PlayerPrefs.GetFloat("CarSevenCoinNeedForWeapon");
                CarSevenWeaponUpdatePercentage = CarSevenWeaponLevel + 10;
                if (coin >= CarSevenCoinNeedForWeapon)
                {
                    coin = coin - CarSevenCoinNeedForWeapon;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarSevenCoinNeedForWeapon = CoinNeed(CarSevenCoinNeedForWeapon, CarSevenWeaponUpdatePercentage);
                    PlayerPrefs.SetFloat("CarSevenCoinNeedForWeapon", CarSevenCoinNeedForWeapon);
                    if (CarSevenWeaponLevel == 9)
                    {
                        WeaponValue.text = "Max";
                        WeaponCoinIcon.SetActive(false);
                    }
                    else
                    {
                        WeaponCoinIcon.SetActive(true);
                        WeaponValue.text = PlayerPrefs.GetFloat("CarSevenCoinNeedForWeapon").ToString();
                    }
                    CarSevenWeaponLevel++;
                    PlayerPrefs.SetInt("CarSevenWeaponLevel", CarSevenWeaponLevel);
                    WeaponLevel.text = PlayerPrefs.GetInt("CarSevenWeaponLevel").ToString() + "/10";
                    CarSevenWeaponDamage = CarSevenWeaponDamage + WeaponUpdateValue;
                    PlayerPrefs.SetFloat("CarSevenWeaponDamage", CarSevenWeaponDamage);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarSevenWeaponCurrentValue: " + CarSevenCoinNeedForWeapon);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarSevenCoinNeedForWeapon + "coin");
                }
            }
        }
    }


    //Calculation of Needed coin for update
    float CoinNeed(float currentValue, int percentage)
    {
        float nextValue = currentValue + (currentValue * 0.1f);
        return Mathf.Round(nextValue);
        //return nextValue;
    }

    IEnumerator InsufficientCoinTime()
    {
        yield return new WaitForSeconds(1f);
        PlayerPrefs.SetInt("InsufficientCoin", 0);
    }
}
