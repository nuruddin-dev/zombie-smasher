﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeScene : CarManager
{
    public float coin;
    new int selectedCar;
    public GameObject Garage, AllCar, StartPanel, LevelSelectPanel, MenuPanel, UpgradePanel, CoinText, CoinIcon;
    public GameObject EngineCoinIcon, WheelCoinIcon, WeaponCoinIcon;
    public GameObject InsufficientCoin;
    private int insufficientCoin;
    public Text EngineValue, EngineLevel;
    public Text WheelValue, WheelLevel;
    public Text WeaponValue, WeaponLevel;
    float CarOneCoinNeedForEngine, CarTwoCoinNeedForEngine, CarThreeCoinNeedForEngine, CarFourCoinNeedForEngine, CarFiveCoinNeedForEngine, CarSixCoinNeedForEngine, CarSevenCoinNeedForEngine;
    int CarOneEngineLevel, CarTwoEngineLevel, CarThreeEngineLevel, CarFourEngineLevel, CarFiveEngineLevel, CarSixEngineLevel, CarSevenEngineLevel;
    float CarOneCoinNeedForWheel, CarTwoCoinNeedForWheel, CarThreeCoinNeedForWheel, CarFourCoinNeedForWheel, CarFiveCoinNeedForWheel, CarSixCoinNeedForWheel, CarSevenCoinNeedForWheel;
    int CarOneWheelLevel, CarTwoWheelLevel, CarThreeWheelLevel, CarFourWheelLevel, CarFiveWheelLevel, CarSixWheelLevel, CarSevenWheelLevel;
    float CarOneCoinNeedForWeapon, CarTwoCoinNeedForWeapon, CarThreeCoinNeedForWeapon, CarFourCoinNeedForWeapon, CarFiveCoinNeedForWeapon, CarSixCoinNeedForWeapon, CarSevenCoinNeedForWeapon;
    int CarOneWeaponLevel, CarTwoWeaponLevel, CarThreeWeaponLevel, CarFourWeaponLevel, CarFiveWeaponLevel, CarSixWeaponLevel, CarSevenWeaponLevel;

    void Awake()
    {
        Garage = GameObject.Find("Garage");
        AllCar = GameObject.Find("AllCar");
        StartPanel = GameObject.Find("StartPanel");
        LevelSelectPanel = GameObject.Find("LevelSelectPanel");
        MenuPanel = GameObject.Find("MenuPanel");
        UpgradePanel = GameObject.Find("UpgradePanel");
        CoinText = GameObject.Find("CoinText");
        CoinIcon = GameObject.Find("CoinIcon");
        InsufficientCoin = GameObject.Find("Insufficient Coin");
        EngineCoinIcon = GameObject.Find("EngineCoinIcon");
        WheelCoinIcon = GameObject.Find("WheelCoinIcon");
        WeaponCoinIcon = GameObject.Find("WeaponCoinIcon");
        EngineValue = GameObject.Find("EngineValue").GetComponent<Text>();
        EngineLevel = GameObject.Find("EngineLevel").GetComponent<Text>();
        WheelValue = GameObject.Find("WheelValue").GetComponent<Text>();
        WheelLevel = GameObject.Find("WheelLevel").GetComponent<Text>();
        WeaponValue = GameObject.Find("WeaponValue").GetComponent<Text>();
        WeaponLevel = GameObject.Find("WeaponLevel").GetComponent<Text>();
    }

    // Start is called before the first frame update
    void Start()
    {
        selectedCar = CarManager.selectedCar;
    }

    // Update is called once per frame
    void Update()
    {
        //CoinText.GetComponent<Text>().text = PlayerPrefs.GetFloat("Coin").ToString();
        if (Input.GetKey(KeyCode.C))
        {
            PlayerPrefs.SetFloat("Coin", 11111f);
        }
        coin = PlayerPrefs.GetFloat("Coin");
        CoinText.GetComponent<Text>().text = coin.ToString();
        selectedCar = CarManager.selectedCar;

        if (Input.GetKey(KeyCode.R))
        {
            Reset();
        }

        if (PlayerPrefs.HasKey("InsufficientCoin"))
            insufficientCoin = PlayerPrefs.GetInt("InsufficientCoin");
        else
            insufficientCoin = 0;

        if (insufficientCoin == 0)
        {
            InsufficientCoin.SetActive(true);
        }
        if(insufficientCoin == 1)
        {
            InsufficientCoin.SetActive(false);
        }
    }

    public void Upgrade()
    {
        //InsufficientCoin.SetActive(false);
        selectedCar = CarManager.selectedCar;
        StartPanel.SetActive(false);
        AllCar.SetActive(false);
        MenuPanel.SetActive(false);
        Garage.SetActive(false);
        CoinText.SetActive(true);
        CoinIcon.SetActive(true);
        UpgradePanel.SetActive(true);
        LevelSelectPanel.SetActive(false);
        EnginePlayerPrefs();
        WheelPlayerPrefs();
        WeaponPlayerPrefs();
        //PlayerPrefs.SetFloat("Coin", coin);
        Debug.Log("Current coin: " + coin);
    }

    public void UpgradeOk()
    {
        Garage.SetActive(true);
        AllCar.SetActive(true);
        MenuPanel.SetActive(true);
        UpgradePanel.SetActive(false);
    }

    //Function called when upgrade button clicked To check PlayerPrefs for Engine
    public void EnginePlayerPrefs()
    {
        if (selectedCar == 0)
        {
            if (PlayerPrefs.HasKey("CarOneCoinNeedForEngine"))
                CarOneCoinNeedForEngine = PlayerPrefs.GetFloat("CarOneCoinNeedForEngine");
            else
            {
                CarOneCoinNeedForEngine = 1100;
                PlayerPrefs.SetFloat("CarOneCoinNeedForEngine", CarOneCoinNeedForEngine);
            }
            if (PlayerPrefs.HasKey("CarOneEngineLevel"))
                CarOneEngineLevel = PlayerPrefs.GetInt("CarOneEngineLevel");
            else
            {
                CarOneEngineLevel = 1;
                PlayerPrefs.SetInt("CarOneEngineLevel", CarOneEngineLevel);
            }
            if (CarOneEngineLevel < 10)
            {
                EngineCoinIcon.SetActive(true);
                EngineValue.text = CarOneCoinNeedForEngine.ToString();
            }
            else
            {
                EngineValue.text = "Max";
                EngineCoinIcon.SetActive(false);
            }
            EngineLevel.text = CarOneEngineLevel.ToString() + "/10";
        }
        if (selectedCar == 1)
        {
            if (PlayerPrefs.HasKey("CarTwoCoinNeedForEngine"))
                CarTwoCoinNeedForEngine = PlayerPrefs.GetFloat("CarTwoCoinNeedForEngine");
            else
            {
                CarTwoCoinNeedForEngine = 1100;
                PlayerPrefs.SetFloat("CarTwoCoinNeedForEngine", CarTwoCoinNeedForEngine);
            }
            if (PlayerPrefs.HasKey("CarTwoEngineLevel"))
                CarTwoEngineLevel = PlayerPrefs.GetInt("CarTwoEngineLevel");
            else
            {
                CarTwoEngineLevel = 1;
                PlayerPrefs.SetInt("CarTwoEngineLevel", CarTwoEngineLevel);
            }
            if (CarTwoEngineLevel < 10)
            {
                EngineCoinIcon.SetActive(true);
                EngineValue.text = CarTwoCoinNeedForEngine.ToString();
            }
            else
            {
                EngineValue.text = "Max";
                EngineCoinIcon.SetActive(false);
            }
            EngineLevel.text = CarTwoEngineLevel.ToString() + "/10";
        }
        if (selectedCar == 2)
        {
            if (PlayerPrefs.HasKey("CarThreeCoinNeedForEngine"))
                CarThreeCoinNeedForEngine = PlayerPrefs.GetFloat("CarThreeCoinNeedForEngine");
            else
            {
                CarThreeCoinNeedForEngine = 1100;
                PlayerPrefs.SetFloat("CarThreeCoinNeedForEngine", CarThreeCoinNeedForEngine);
            }
            if (PlayerPrefs.HasKey("CarThreeEngineLevel"))
                CarThreeEngineLevel = PlayerPrefs.GetInt("CarThreeEngineLevel");
            else
            {
                CarThreeEngineLevel = 1;
                PlayerPrefs.SetInt("CarThreeEngineLevel", CarThreeEngineLevel);
            }
            if (CarThreeEngineLevel < 10)
            {
                EngineCoinIcon.SetActive(true);
                EngineValue.text = CarThreeCoinNeedForEngine.ToString();
            }
            else
            {
                EngineValue.text = "Max";
                EngineCoinIcon.SetActive(false);
            }
            EngineLevel.text = CarThreeEngineLevel.ToString() + "/10";
        }
        if (selectedCar == 3)
        {
            if (PlayerPrefs.HasKey("CarFourCoinNeedForEngine"))
                CarFourCoinNeedForEngine = PlayerPrefs.GetFloat("CarFourCoinNeedForEngine");
            else
            {
                CarFourCoinNeedForEngine = 1100;
                PlayerPrefs.SetFloat("CarFourCoinNeedForEngine", CarFourCoinNeedForEngine);
            }
            if (PlayerPrefs.HasKey("CarFourEngineLevel"))
                CarFourEngineLevel = PlayerPrefs.GetInt("CarFourEngineLevel");
            else
            {
                CarFourEngineLevel = 1;
                PlayerPrefs.SetInt("CarFourEngineLevel", CarFourEngineLevel);
            }
            if (CarFourEngineLevel < 10)
            {
                EngineCoinIcon.SetActive(true);
                EngineValue.text = CarFourCoinNeedForEngine.ToString();
            }
            else
            {
                EngineValue.text = "Max";
                EngineCoinIcon.SetActive(false);
            }
            EngineLevel.text = CarFourEngineLevel.ToString() + "/10";
        }
        if (selectedCar == 4)
        {
            if (PlayerPrefs.HasKey("CarFiveCoinNeedForEngine"))
                CarFiveCoinNeedForEngine = PlayerPrefs.GetFloat("CarFiveCoinNeedForEngine");
            else
            {
                CarFiveCoinNeedForEngine = 1100;
                PlayerPrefs.SetFloat("CarFiveCoinNeedForEngine", CarFiveCoinNeedForEngine);
            }
            if (PlayerPrefs.HasKey("CarFiveEngineLevel"))
                CarFiveEngineLevel = PlayerPrefs.GetInt("CarFiveEngineLevel");
            else
            {
                CarFiveEngineLevel = 1;
                PlayerPrefs.SetInt("CarFiveEngineLevel", CarFiveEngineLevel);
            }
            if (CarFiveEngineLevel < 10)
            {
                EngineCoinIcon.SetActive(true);
                EngineValue.text = CarFiveCoinNeedForEngine.ToString();
            }
            else
            {
                EngineValue.text = "Max";
                EngineCoinIcon.SetActive(false);
            }
            EngineLevel.text = CarFiveEngineLevel.ToString() + "/10";
        }
        if (selectedCar == 5)
        {
            if (PlayerPrefs.HasKey("CarSixCoinNeedForEngine"))
                CarSixCoinNeedForEngine = PlayerPrefs.GetFloat("CarSixCoinNeedForEngine");
            else
            {
                CarSixCoinNeedForEngine = 1100;
                PlayerPrefs.SetFloat("CarSixCoinNeedForEngine", CarSixCoinNeedForEngine);
            }
            if (PlayerPrefs.HasKey("CarSixEngineLevel"))
                CarSixEngineLevel = PlayerPrefs.GetInt("CarSixEngineLevel");
            else
            {
                CarSixEngineLevel = 1;
                PlayerPrefs.SetInt("CarSixEngineLevel", CarSixEngineLevel);
            }
            if (CarSixEngineLevel < 10)
            {
                EngineCoinIcon.SetActive(true);
                EngineValue.text = CarSixCoinNeedForEngine.ToString();
            }
            else
            {
                EngineValue.text = "Max";
                EngineCoinIcon.SetActive(false);
            }
            EngineLevel.text = CarSixEngineLevel.ToString() + "/10";
        }
        if (selectedCar == 6)
        {
            if (PlayerPrefs.HasKey("CarSevenCoinNeedForEngine"))
                CarSevenCoinNeedForEngine = PlayerPrefs.GetFloat("CarSevenCoinNeedForEngine");
            else
            {
                CarSevenCoinNeedForEngine = 1100;
                PlayerPrefs.SetFloat("CarSevenCoinNeedForEngine", CarSevenCoinNeedForEngine);
            }
            if (PlayerPrefs.HasKey("CarSevenEngineLevel"))
                CarSevenEngineLevel = PlayerPrefs.GetInt("CarSevenEngineLevel");
            else
            {
                CarSevenEngineLevel = 1;
                PlayerPrefs.SetInt("CarSevenEngineLevel", CarSevenEngineLevel);
            }
            if (CarSevenEngineLevel < 10)
            {
                EngineCoinIcon.SetActive(true);
                EngineValue.text = CarSevenCoinNeedForEngine.ToString();
            }
            else
            {
                EngineValue.text = "Max";
                EngineCoinIcon.SetActive(false);
            }
            EngineLevel.text = CarSevenEngineLevel.ToString() + "/10";
        }
    }

    //Function called when upgrade button clicked To check PlayerPrefs for Wheel
    public void WheelPlayerPrefs()
    {
        if (selectedCar == 0)
        {
            if (PlayerPrefs.HasKey("CarOneCoinNeedForWheel"))
                CarOneCoinNeedForWheel = PlayerPrefs.GetFloat("CarOneCoinNeedForWheel");
            else
            {
                CarOneCoinNeedForWheel = 1100;
                PlayerPrefs.SetFloat("CarOneCoinNeedForWheel", CarOneCoinNeedForWheel);
            }
            if (PlayerPrefs.HasKey("CarOneWheelLevel"))
                CarOneWheelLevel = PlayerPrefs.GetInt("CarOneWheelLevel");
            else
            {
                CarOneWheelLevel = 1;
                PlayerPrefs.SetInt("CarOneWheelLevel", CarOneWheelLevel);
            }
            if (CarOneWheelLevel < 10)
            {
                WheelCoinIcon.SetActive(true);
                WheelValue.text = CarOneCoinNeedForWheel.ToString();
            }
            else
            {
                WheelValue.text = "Max";
                WheelCoinIcon.SetActive(false);
            }
            WheelLevel.text = CarOneWheelLevel.ToString() + "/10";
        }
        if (selectedCar == 1)
        {
            if (PlayerPrefs.HasKey("CarTwoCoinNeedForWheel"))
                CarTwoCoinNeedForWheel = PlayerPrefs.GetFloat("CarTwoCoinNeedForWheel");
            else
            {
                CarTwoCoinNeedForWheel = 1100;
                PlayerPrefs.SetFloat("CarTwoCoinNeedForWheel", CarTwoCoinNeedForWheel);
            }
            if (PlayerPrefs.HasKey("CarTwoWheelLevel"))
                CarTwoWheelLevel = PlayerPrefs.GetInt("CarTwoWheelLevel");
            else
            {
                CarTwoWheelLevel = 1;
                PlayerPrefs.SetInt("CarTwoWheelLevel", CarTwoWheelLevel);
            }
            if (CarTwoWheelLevel < 10)
            {
                WheelCoinIcon.SetActive(true);
                WheelValue.text = CarTwoCoinNeedForWheel.ToString();
            }
            else
            {
                WheelValue.text = "Max";
                WheelCoinIcon.SetActive(false);
            }
            WheelLevel.text = CarTwoWheelLevel.ToString() + "/10";
        }
        if (selectedCar == 2)
        {
            if (PlayerPrefs.HasKey("CarThreeCoinNeedForWheel"))
                CarThreeCoinNeedForWheel = PlayerPrefs.GetFloat("CarThreeCoinNeedForWheel");
            else
            {
                CarThreeCoinNeedForWheel = 1100;
                PlayerPrefs.SetFloat("CarThreeCoinNeedForWheel", CarThreeCoinNeedForWheel);
            }
            if (PlayerPrefs.HasKey("CarThreeWheelLevel"))
                CarThreeWheelLevel = PlayerPrefs.GetInt("CarThreeWheelLevel");
            else
            {
                CarThreeWheelLevel = 1;
                PlayerPrefs.SetInt("CarThreeWheelLevel", CarThreeWheelLevel);
            }
            if (CarThreeWheelLevel < 10)
            {
                WheelCoinIcon.SetActive(true);
                WheelValue.text = CarThreeCoinNeedForWheel.ToString();
            }
            else
            {
                WheelValue.text = "Max";
                WheelCoinIcon.SetActive(false);
            }
            WheelLevel.text = CarThreeWheelLevel.ToString() + "/10";
        }
        if (selectedCar == 3)
        {
            if (PlayerPrefs.HasKey("CarFourCoinNeedForWheel"))
                CarFourCoinNeedForWheel = PlayerPrefs.GetFloat("CarFourCoinNeedForWheel");
            else
            {
                CarFourCoinNeedForWheel = 1100;
                PlayerPrefs.SetFloat("CarFourCoinNeedForWheel", CarFourCoinNeedForWheel);
            }
            if (PlayerPrefs.HasKey("CarFourWheelLevel"))
                CarFourWheelLevel = PlayerPrefs.GetInt("CarFourWheelLevel");
            else
            {
                CarFourWheelLevel = 1;
                PlayerPrefs.SetInt("CarFourWheelLevel", CarFourWheelLevel);
            }
            if (CarFourWheelLevel < 10)
            {
                WheelCoinIcon.SetActive(true);
                WheelValue.text = CarFourCoinNeedForWheel.ToString();
            }
            else
            {
                WheelValue.text = "Max";
                WheelCoinIcon.SetActive(false);
            }
            WheelLevel.text = CarFourWheelLevel.ToString() + "/10";
        }
        if (selectedCar == 4)
        {
            if (PlayerPrefs.HasKey("CarFiveCoinNeedForWheel"))
                CarFiveCoinNeedForWheel = PlayerPrefs.GetFloat("CarFiveCoinNeedForWheel");
            else
            {
                CarFiveCoinNeedForWheel = 1100;
                PlayerPrefs.SetFloat("CarFiveCoinNeedForWheel", CarFiveCoinNeedForWheel);
            }
            if (PlayerPrefs.HasKey("CarFiveWheelLevel"))
                CarFiveWheelLevel = PlayerPrefs.GetInt("CarFiveWheelLevel");
            else
            {
                CarFiveWheelLevel = 1;
                PlayerPrefs.SetInt("CarFiveWheelLevel", CarFiveWheelLevel);
            }
            if (CarFiveWheelLevel < 10)
            {
                WheelCoinIcon.SetActive(true);
                WheelValue.text = CarFiveCoinNeedForWheel.ToString();
            }
            else
            {
                WheelValue.text = "Max";
                WheelCoinIcon.SetActive(false);
            }
            WheelLevel.text = CarFiveWheelLevel.ToString() + "/10";
        }
        if (selectedCar == 5)
        {
            if (PlayerPrefs.HasKey("CarSixCoinNeedForWheel"))
                CarSixCoinNeedForWheel = PlayerPrefs.GetFloat("CarSixCoinNeedForWheel");
            else
            {
                CarSixCoinNeedForWheel = 1100;
                PlayerPrefs.SetFloat("CarSixCoinNeedForWheel", CarSixCoinNeedForWheel);
            }
            if (PlayerPrefs.HasKey("CarSixWheelLevel"))
                CarSixWheelLevel = PlayerPrefs.GetInt("CarSixWheelLevel");
            else
            {
                CarSixWheelLevel = 1;
                PlayerPrefs.SetInt("CarSixWheelLevel", CarSixWheelLevel);
            }
            if (CarSixWheelLevel < 10)
            {
                WheelCoinIcon.SetActive(true);
                WheelValue.text = CarSixCoinNeedForWheel.ToString();
            }
            else
            {
                WheelValue.text = "Max";
                WheelCoinIcon.SetActive(false);
            }
            WheelLevel.text = CarSixWheelLevel.ToString() + "/10";
        }
        if (selectedCar == 6)
        {
            if (PlayerPrefs.HasKey("CarSevenCoinNeedForWheel"))
                CarSevenCoinNeedForWheel = PlayerPrefs.GetFloat("CarSevenCoinNeedForWheel");
            else
            {
                CarSevenCoinNeedForWheel = 1100;
                PlayerPrefs.SetFloat("CarSevenCoinNeedForWheel", CarSevenCoinNeedForWheel);
            }
            if (PlayerPrefs.HasKey("CarSevenWheelLevel"))
                CarSevenWheelLevel = PlayerPrefs.GetInt("CarSevenWheelLevel");
            else
            {
                CarSevenWheelLevel = 1;
                PlayerPrefs.SetInt("CarSevenWheelLevel", CarSevenWheelLevel);
            }
            if (CarSevenWheelLevel < 10)
            {
                WheelCoinIcon.SetActive(true);
                WheelValue.text = CarSevenCoinNeedForWheel.ToString();
            }
            else
            {
                WheelValue.text = "Max";
                WheelCoinIcon.SetActive(false);
            }
            WheelLevel.text = CarSevenWheelLevel.ToString() + "/10";
        }
    }

    //Function called when upgrade button clicked To check PlayerPrefs for Weapon
    public void WeaponPlayerPrefs()
    {
        if (selectedCar == 0)
        {
            if (PlayerPrefs.HasKey("CarOneCoinNeedForWeapon"))
                CarOneCoinNeedForWeapon = PlayerPrefs.GetFloat("CarOneCoinNeedForWeapon");
            else
            {
                CarOneCoinNeedForWeapon = 1100;
                PlayerPrefs.SetFloat("CarOneCoinNeedForWeapon", CarOneCoinNeedForWeapon);
            }
            if (PlayerPrefs.HasKey("CarOneWeaponLevel"))
                CarOneWeaponLevel = PlayerPrefs.GetInt("CarOneWeaponLevel");
            else
            {
                CarOneWeaponLevel = 1;
                PlayerPrefs.SetInt("CarOneWeaponLevel", CarOneWeaponLevel);
            }
            if (CarOneWeaponLevel < 10)
            {
                WeaponCoinIcon.SetActive(true);
                WeaponValue.text = CarOneCoinNeedForWeapon.ToString();
            }
            else
            {
                WeaponValue.text = "Max";
                WeaponCoinIcon.SetActive(false);
            }
            WeaponLevel.text = CarOneWeaponLevel.ToString() + "/10";
        }
        if (selectedCar == 1)
        {
            if (PlayerPrefs.HasKey("CarTwoCoinNeedForWeapon"))
                CarTwoCoinNeedForWeapon = PlayerPrefs.GetFloat("CarTwoCoinNeedForWeapon");
            else
            {
                CarTwoCoinNeedForWeapon = 1100;
                PlayerPrefs.SetFloat("CarTwoCoinNeedForWeapon", CarTwoCoinNeedForWeapon);
            }
            if (PlayerPrefs.HasKey("CarTwoWeaponLevel"))
                CarTwoWeaponLevel = PlayerPrefs.GetInt("CarTwoWeaponLevel");
            else
            {
                CarTwoWeaponLevel = 1;
                PlayerPrefs.SetInt("CarTwoWeaponLevel", CarTwoWeaponLevel);
            }
            if (CarTwoWeaponLevel < 10)
            {
                WeaponCoinIcon.SetActive(true);
                WeaponValue.text = CarTwoCoinNeedForWeapon.ToString();
            }
            else
            {
                WeaponValue.text = "Max";
                WeaponCoinIcon.SetActive(false);
            }
            WeaponLevel.text = CarTwoWeaponLevel.ToString() + "/10";
        }
        if (selectedCar == 2)
        {
            if (PlayerPrefs.HasKey("CarThreeCoinNeedForWeapon"))
                CarThreeCoinNeedForWeapon = PlayerPrefs.GetFloat("CarThreeCoinNeedForWeapon");
            else
            {
                CarThreeCoinNeedForWeapon = 1100;
                PlayerPrefs.SetFloat("CarThreeCoinNeedForWeapon", CarThreeCoinNeedForWeapon);
            }
            if (PlayerPrefs.HasKey("CarThreeWeaponLevel"))
                CarThreeWeaponLevel = PlayerPrefs.GetInt("CarThreeWeaponLevel");
            else
            {
                CarThreeWeaponLevel = 1;
                PlayerPrefs.SetInt("CarThreeWeaponLevel", CarThreeWeaponLevel);
            }
            if (CarThreeWeaponLevel < 10)
            {
                WeaponCoinIcon.SetActive(true);
                WeaponValue.text = CarThreeCoinNeedForWeapon.ToString();
            }
            else
            {
                WeaponValue.text = "Max";
                WeaponCoinIcon.SetActive(false);
            }
            WeaponLevel.text = CarThreeWeaponLevel.ToString() + "/10";
        }
        if (selectedCar == 3)
        {
            if (PlayerPrefs.HasKey("CarFourCoinNeedForWeapon"))
                CarFourCoinNeedForWeapon = PlayerPrefs.GetFloat("CarFourCoinNeedForWeapon");
            else
            {
                CarFourCoinNeedForWeapon = 1100;
                PlayerPrefs.SetFloat("CarFourCoinNeedForWeapon", CarFourCoinNeedForWeapon);
            }
            if (PlayerPrefs.HasKey("CarFourWeaponLevel"))
                CarFourWeaponLevel = PlayerPrefs.GetInt("CarFourWeaponLevel");
            else
            {
                CarFourWeaponLevel = 1;
                PlayerPrefs.SetInt("CarFourWeaponLevel", CarFourWeaponLevel);
            }
            if (CarFourWeaponLevel < 10)
            {
                WeaponCoinIcon.SetActive(true);
                WeaponValue.text = CarFourCoinNeedForWeapon.ToString();
            }
            else
            {
                WeaponValue.text = "Max";
                WeaponCoinIcon.SetActive(false);
            }
            WeaponLevel.text = CarFourWeaponLevel.ToString() + "/10";
        }
        if (selectedCar == 4)
        {
            if (PlayerPrefs.HasKey("CarFiveCoinNeedForWeapon"))
                CarFiveCoinNeedForWeapon = PlayerPrefs.GetFloat("CarFiveCoinNeedForWeapon");
            else
            {
                CarFiveCoinNeedForWeapon = 1100;
                PlayerPrefs.SetFloat("CarFiveCoinNeedForWeapon", CarFiveCoinNeedForWeapon);
            }
            if (PlayerPrefs.HasKey("CarFiveWeaponLevel"))
                CarFiveWeaponLevel = PlayerPrefs.GetInt("CarFiveWeaponLevel");
            else
            {
                CarFiveWeaponLevel = 1;
                PlayerPrefs.SetInt("CarFiveWeaponLevel", CarFiveWeaponLevel);
            }
            if (CarFiveWeaponLevel < 10)
            {
                WeaponCoinIcon.SetActive(true);
                WeaponValue.text = CarFiveCoinNeedForWeapon.ToString();
            }
            else
            {
                WeaponValue.text = "Max";
                WeaponCoinIcon.SetActive(false);
            }
            WeaponLevel.text = CarFiveWeaponLevel.ToString() + "/10";
        }
        if (selectedCar == 5)
        {
            if (PlayerPrefs.HasKey("CarSixCoinNeedForWeapon"))
                CarSixCoinNeedForWeapon = PlayerPrefs.GetFloat("CarSixCoinNeedForWeapon");
            else
            {
                CarSixCoinNeedForWeapon = 1100;
                PlayerPrefs.SetFloat("CarSixCoinNeedForWeapon", CarSixCoinNeedForWeapon);
            }
            if (PlayerPrefs.HasKey("CarSixWeaponLevel"))
                CarSixWeaponLevel = PlayerPrefs.GetInt("CarSixWeaponLevel");
            else
            {
                CarSixWeaponLevel = 1;
                PlayerPrefs.SetInt("CarSixWeaponLevel", CarSixWeaponLevel);
            }
            if (CarSixWeaponLevel < 10)
            {
                WeaponCoinIcon.SetActive(true);
                WeaponValue.text = CarSixCoinNeedForWeapon.ToString();
            }
            else
            {
                WeaponValue.text = "Max";
                WeaponCoinIcon.SetActive(false);
            }
            WeaponLevel.text = CarSixWeaponLevel.ToString() + "/10";
        } 
        if (selectedCar == 6)
        {
            if (PlayerPrefs.HasKey("CarSevenCoinNeedForWeapon"))
                CarSevenCoinNeedForWeapon = PlayerPrefs.GetFloat("CarSevenCoinNeedForWeapon");
            else
            {
                CarSevenCoinNeedForWeapon = 1100;
                PlayerPrefs.SetFloat("CarSevenCoinNeedForWeapon", CarSevenCoinNeedForWeapon);
            }
            if (PlayerPrefs.HasKey("CarSevenWeaponLevel"))
                CarSevenWeaponLevel = PlayerPrefs.GetInt("CarSevenWeaponLevel");
            else
            {
                CarSevenWeaponLevel = 1;
                PlayerPrefs.SetInt("CarSevenWeaponLevel", CarSevenWeaponLevel);
            }
            if (CarSevenWeaponLevel < 10)
            {
                WeaponCoinIcon.SetActive(true);
                WeaponValue.text = CarSevenCoinNeedForWeapon.ToString();
            }
            else
            {
                WeaponValue.text = "Max";
                WeaponCoinIcon.SetActive(false);
            }
            WeaponLevel.text = CarSevenWeaponLevel.ToString() + "/10";
        }
    }

    //Reset all PlayerPrefs value
    public void Reset()
    {
        Debug.Log("Ressetall");
        PlayerPrefs.DeleteAll();
    }
}
