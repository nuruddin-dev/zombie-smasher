﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WheelUpgrade : CarManager
{
    new int selectedCar;
    float coin;
    public Text WheelValue, WheelLevel;
    public GameObject WheelCoinIcon;
    int CarOneWheelUpdatePercentage, CarTwoWheelUpdatePercentage, CarThreeWheelUpdatePercentage, CarFourWheelUpdatePercentage, CarFiveWheelUpdatePercentage, CarSixWheelUpdatePercentage, CarSevenWheelUpdatePercentage;
    float CarOneWheelCurrentValue, CarTwoWheelCurrentValue, CarThreeWheelCurrentValue, CarFourWheelCurrentValue, CarFiveWheelCurrentValue, CarSixWheelCurrentValue, CarSevenWheelCurrentValue;
    float CarOneCoinNeedForWheel, CarTwoCoinNeedForWheel, CarThreeCoinNeedForWheel, CarFourCoinNeedForWheel, CarFiveCoinNeedForWheel, CarSixCoinNeedForWheel, CarSevenCoinNeedForWheel;
    int CarOneWheelLevel, CarTwoWheelLevel, CarThreeWheelLevel, CarFourWheelLevel, CarFiveWheelLevel, CarSixWheelLevel, CarSevenWheelLevel;
    float CarOneFriction, CarTwoFriction, CarThreeFriction, CarFourFriction, CarFiveFriction, CarSixFriction, CarSevenFriction;

    void Awake()
    {
        WheelCoinIcon = GameObject.Find("WheelCoinIcon");
        WheelValue = GameObject.Find("WheelValue").GetComponent<Text>();
        WheelLevel = GameObject.Find("WheelLevel").GetComponent<Text>();
    }
    //Upgrade All Car Wheel
    public void UpgradeWheel()
    {
        //selectedCar = CarManager.selectedCar;
        //selectedCar = GameObject.Find("CarManager").GetComponent<CarManager>().selectedCar;
        selectedCar = CarManager.selectedCar;

        coin = PlayerPrefs.GetFloat("Coin");
        CarOneWheelLevel = PlayerPrefs.GetInt("CarOneWheelLevel");
        CarTwoWheelLevel = PlayerPrefs.GetInt("CarTwoWheelLevel");
        CarThreeWheelLevel = PlayerPrefs.GetInt("CarThreeWheelLevel");
        CarFourWheelLevel = PlayerPrefs.GetInt("CarFourWheelLevel");
        CarFiveWheelLevel = PlayerPrefs.GetInt("CarFiveWheelLevel");
        CarSixWheelLevel = PlayerPrefs.GetInt("CarSixWheelLevel");
        CarSevenWheelLevel = PlayerPrefs.GetInt("CarSevenWheelLevel");


        int WheelUpdateValue = 500;

        if (selectedCar == 0)
        {
            Debug.Log("Car one Wheel level : " + CarOneWheelLevel);
            if (CarOneWheelLevel < 10)
            {
                CarOneCoinNeedForWheel = PlayerPrefs.GetFloat("CarOneCoinNeedForWheel");
                CarOneWheelUpdatePercentage = CarOneWheelLevel + 10;
                if (coin >= CarOneCoinNeedForWheel)
                {
                    coin = coin - CarOneCoinNeedForWheel;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarOneCoinNeedForWheel = CoinNeed(CarOneCoinNeedForWheel, CarOneWheelUpdatePercentage);
                    PlayerPrefs.SetFloat("CarOneCoinNeedForWheel", CarOneCoinNeedForWheel);
                    if (CarOneWheelLevel == 9)
                    {
                        WheelCoinIcon.SetActive(false);
                        WheelValue.text = "Max";
                    }
                    else
                    {
                        WheelCoinIcon.SetActive(true);
                        WheelValue.text = PlayerPrefs.GetFloat("CarOneCoinNeedForWheel").ToString();
                    }
                    CarOneWheelLevel++;
                    PlayerPrefs.SetInt("CarOneWheelLevel", CarOneWheelLevel);
                    WheelLevel.text = PlayerPrefs.GetInt("CarOneWheelLevel").ToString() + "/10";
                    CarOneFriction = CarOneFriction + WheelUpdateValue;
                    PlayerPrefs.SetFloat("CarOneFriction", CarOneFriction);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarOneWheelCurrentValue: " + CarOneCoinNeedForWheel);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarOneCoinNeedForWheel + "coin");
                }
            }
        }
        if (selectedCar == 1)
        {
            Debug.Log("Car Two Wheel level : " + CarTwoWheelLevel);
            if (CarTwoWheelLevel < 10)
            {
                CarTwoCoinNeedForWheel = PlayerPrefs.GetFloat("CarTwoCoinNeedForWheel");
                CarTwoWheelUpdatePercentage = CarTwoWheelLevel + 10;
                if (coin >= CarTwoCoinNeedForWheel)
                {
                    coin = coin - CarTwoCoinNeedForWheel;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarTwoCoinNeedForWheel = CoinNeed(CarTwoCoinNeedForWheel, CarTwoWheelUpdatePercentage);
                    PlayerPrefs.SetFloat("CarTwoCoinNeedForWheel", CarTwoCoinNeedForWheel);
                    if (CarTwoWheelLevel == 9)
                    {
                        WheelCoinIcon.SetActive(false);
                        WheelValue.text = "Max";
                    }
                    else
                    {
                        WheelCoinIcon.SetActive(true);
                        WheelValue.text = PlayerPrefs.GetFloat("CarTwoCoinNeedForWheel").ToString();
                    }
                    CarTwoWheelLevel++;
                    PlayerPrefs.SetInt("CarTwoWheelLevel", CarTwoWheelLevel);
                    WheelLevel.text = PlayerPrefs.GetInt("CarTwoWheelLevel").ToString() + "/10";
                    CarTwoFriction = CarTwoFriction + WheelUpdateValue;
                    PlayerPrefs.SetFloat("CarTwoFriction", CarTwoFriction);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarTwoWheelCurrentValue: " + CarTwoCoinNeedForWheel);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarTwoCoinNeedForWheel + "coin");
                }
            }
        }
        if (selectedCar == 2)
        {
            Debug.Log("Car Three Wheel level : " + CarThreeWheelLevel);
            if (CarThreeWheelLevel < 10)
            {
                CarThreeCoinNeedForWheel = PlayerPrefs.GetFloat("CarThreeCoinNeedForWheel");
                CarThreeWheelUpdatePercentage = CarThreeWheelLevel + 10;
                if (coin >= CarThreeCoinNeedForWheel)
                {
                    coin = coin - CarThreeCoinNeedForWheel;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarThreeCoinNeedForWheel = CoinNeed(CarThreeCoinNeedForWheel, CarThreeWheelUpdatePercentage);
                    PlayerPrefs.SetFloat("CarThreeCoinNeedForWheel", CarThreeCoinNeedForWheel);
                    if (CarThreeWheelLevel == 9)
                    {
                        WheelCoinIcon.SetActive(false);
                        WheelValue.text = "Max";
                    }
                    else
                    {
                        WheelCoinIcon.SetActive(true);
                        WheelValue.text = PlayerPrefs.GetFloat("CarThreeCoinNeedForWheel").ToString();
                    }
                    CarThreeWheelLevel++;
                    PlayerPrefs.SetInt("CarThreeWheelLevel", CarThreeWheelLevel);
                    WheelLevel.text = PlayerPrefs.GetInt("CarThreeWheelLevel").ToString() + "/10";
                    CarThreeFriction = CarThreeFriction + WheelUpdateValue;
                    PlayerPrefs.SetFloat("CarThreeFriction", CarThreeFriction);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarThreeWheelCurrentValue: " + CarThreeCoinNeedForWheel);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarThreeCoinNeedForWheel + "coin");
                }
            }
        }
        if (selectedCar == 3)
        {
            Debug.Log("Car Four Wheel level : " + CarFourWheelLevel);
            if (CarFourWheelLevel < 10)
            {
                CarFourCoinNeedForWheel = PlayerPrefs.GetFloat("CarFourCoinNeedForWheel");
                CarFourWheelUpdatePercentage = CarFourWheelLevel + 10;
                if (coin >= CarFourCoinNeedForWheel)
                {
                    coin = coin - CarFourCoinNeedForWheel;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarFourCoinNeedForWheel = CoinNeed(CarFourCoinNeedForWheel, CarFourWheelUpdatePercentage);
                    PlayerPrefs.SetFloat("CarFourCoinNeedForWheel", CarFourCoinNeedForWheel);
                    if (CarFourWheelLevel == 9)
                    {
                        WheelCoinIcon.SetActive(false);
                        WheelValue.text = "Max";
                    }
                    else
                    {
                        WheelCoinIcon.SetActive(true);
                        WheelValue.text = PlayerPrefs.GetFloat("CarFourCoinNeedForWheel").ToString();
                    }
                    CarFourWheelLevel++;
                    PlayerPrefs.SetInt("CarFourWheelLevel", CarFourWheelLevel);
                    WheelLevel.text = PlayerPrefs.GetInt("CarFourWheelLevel").ToString() + "/10";
                    CarFourFriction = CarFourFriction + WheelUpdateValue;
                    PlayerPrefs.SetFloat("CarFourFriction", CarFourFriction);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarFourWheelCurrentValue: " + CarFourCoinNeedForWheel);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarFourCoinNeedForWheel + "coin");
                }
            }
        }
        if (selectedCar == 4)
        {
            Debug.Log("Car Five Wheel level : " + CarFiveWheelLevel);
            if (CarFiveWheelLevel < 10)
            {
                CarFiveCoinNeedForWheel = PlayerPrefs.GetFloat("CarFiveCoinNeedForWheel");
                CarFiveWheelUpdatePercentage = CarFiveWheelLevel + 10;
                if (coin >= CarFiveCoinNeedForWheel)
                {
                    coin = coin - CarFiveCoinNeedForWheel;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarFiveCoinNeedForWheel = CoinNeed(CarFiveCoinNeedForWheel, CarFiveWheelUpdatePercentage);
                    PlayerPrefs.SetFloat("CarFiveCoinNeedForWheel", CarFiveCoinNeedForWheel);
                    if (CarFiveWheelLevel == 9)
                    {
                        WheelCoinIcon.SetActive(false);
                        WheelValue.text = "Max";
                    }
                    else
                    {
                        WheelCoinIcon.SetActive(true);
                        WheelValue.text = PlayerPrefs.GetFloat("CarFiveCoinNeedForWheel").ToString();
                    }
                    CarFiveWheelLevel++;
                    PlayerPrefs.SetInt("CarFiveWheelLevel", CarFiveWheelLevel);
                    WheelLevel.text = PlayerPrefs.GetInt("CarFiveWheelLevel").ToString() + "/10";
                    CarFiveFriction = CarFiveFriction + WheelUpdateValue;
                    PlayerPrefs.SetFloat("CarFiveFriction", CarFiveFriction);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarFiveWheelCurrentValue: " + CarFiveCoinNeedForWheel);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarFiveCoinNeedForWheel + "coin");
                }
            }
        }
        if (selectedCar == 5)
        {
            Debug.Log("Car Six Wheel level : " + CarSixWheelLevel);
            if (CarSixWheelLevel < 10)
            {
                CarSixCoinNeedForWheel = PlayerPrefs.GetFloat("CarSixCoinNeedForWheel");
                CarSixWheelUpdatePercentage = CarSixWheelLevel + 10;
                if (coin >= CarSixCoinNeedForWheel)
                {
                    coin = coin - CarSixCoinNeedForWheel;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarSixCoinNeedForWheel = CoinNeed(CarSixCoinNeedForWheel, CarSixWheelUpdatePercentage);
                    PlayerPrefs.SetFloat("CarSixCoinNeedForWheel", CarSixCoinNeedForWheel);
                    if (CarSixWheelLevel == 9)
                    {
                        WheelCoinIcon.SetActive(false);
                        WheelValue.text = "Max";
                    }
                    else
                    {
                        WheelCoinIcon.SetActive(true);
                        WheelValue.text = PlayerPrefs.GetFloat("CarSixCoinNeedForWheel").ToString();
                    }
                    CarSixWheelLevel++;
                    PlayerPrefs.SetInt("CarSixWheelLevel", CarSixWheelLevel);
                    WheelLevel.text = PlayerPrefs.GetInt("CarSixWheelLevel").ToString() + "/10";
                    CarSixFriction = CarSixFriction + WheelUpdateValue;
                    PlayerPrefs.SetFloat("CarSixFriction", CarSixFriction);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarSixWheelCurrentValue: " + CarSixCoinNeedForWheel);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarSixCoinNeedForWheel + "coin");
                }
            }
        }
        if (selectedCar == 6)
        {
            Debug.Log("Car Seven Wheel level : " + CarSevenWheelLevel);
            if (CarSevenWheelLevel < 10)
            {
                CarSevenCoinNeedForWheel = PlayerPrefs.GetFloat("CarSevenCoinNeedForWheel");
                CarSevenWheelUpdatePercentage = CarSevenWheelLevel + 10;
                if (coin >= CarSevenCoinNeedForWheel)
                {
                    coin = coin - CarSevenCoinNeedForWheel;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarSevenCoinNeedForWheel = CoinNeed(CarSevenCoinNeedForWheel, CarSevenWheelUpdatePercentage);
                    PlayerPrefs.SetFloat("CarSevenCoinNeedForWheel", CarSevenCoinNeedForWheel);
                    if (CarSevenWheelLevel == 9)
                    {
                        WheelCoinIcon.SetActive(false);
                        WheelValue.text = "Max";
                    }
                    else
                    {
                        WheelCoinIcon.SetActive(true);
                        WheelValue.text = PlayerPrefs.GetFloat("CarSevenCoinNeedForWheel").ToString();
                    }
                    CarSevenWheelLevel++;
                    PlayerPrefs.SetInt("CarSevenWheelLevel", CarSevenWheelLevel);
                    WheelLevel.text = PlayerPrefs.GetInt("CarSevenWheelLevel").ToString() + "/10";
                    CarSevenFriction = CarSevenFriction + WheelUpdateValue;
                    PlayerPrefs.SetFloat("CarSevenFriction", CarSevenFriction);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarSevenWheelCurrentValue: " + CarSevenCoinNeedForWheel);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarSevenCoinNeedForWheel + "coin");
                }
            }
        }
    }


    //Calculation of Needed coin for update
    float CoinNeed(float currentValue, int percentage)
    {
        float nextValue = currentValue + (currentValue * 0.1f);
        return Mathf.Round(nextValue);
        //return nextValue;
    }

    IEnumerator InsufficientCoinTime()
    {
        yield return new WaitForSeconds(.3f);
        PlayerPrefs.SetInt("InsufficientCoin", 0);
    }
}
