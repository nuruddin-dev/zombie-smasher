﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EngineUpgrade : CarManager
{
    new int selectedCar;
    float coin;
    public Text EngineValue, EngineLevel;
    public GameObject EngineCoinIcon;
    int CarOneEngineUpdatePercentage, CarTwoEngineUpdatePercentage, CarThreeEngineUpdatePercentage, CarFourEngineUpdatePercentage, CarFiveEngineUpdatePercentage, CarSixEngineUpdatePercentage, CarSevenEngineUpdatePercentage;
    float CarOneEngineCurrentValue, CarTwoEngineCurrentValue, CarThreeEngineCurrentValue, CarFourEngineCurrentValue, CarFiveEngineCurrentValue, CarSixEngineCurrentValue, CarSevenEngineCurrentValue;
    float CarOneCoinNeedForEngine, CarTwoCoinNeedForEngine, CarThreeCoinNeedForEngine, CarFourCoinNeedForEngine, CarFiveCoinNeedForEngine, CarSixCoinNeedForEngine, CarSevenCoinNeedForEngine;
    int CarOneEngineLevel, CarTwoEngineLevel, CarThreeEngineLevel, CarFourEngineLevel, CarFiveEngineLevel, CarSixEngineLevel, CarSevenEngineLevel;
    int CarOneSpeed, CarTwoSpeed, CarThreeSpeed, CarFourSpeed, CarFiveSpeed, CarSixSpeed, CarSevenSpeed;

    void Awake()
    {
        EngineCoinIcon = GameObject.Find("EngineCoinIcon");
        EngineValue = GameObject.Find("EngineValue").GetComponent<Text>();
        EngineLevel = GameObject.Find("EngineLevel").GetComponent<Text>();
    }
    //Upgrade All Car Engine
    public void UpgradeEngine()
    {
        selectedCar = CarManager.selectedCar;

        coin = PlayerPrefs.GetFloat("Coin");
        CarOneEngineLevel = PlayerPrefs.GetInt("CarOneEngineLevel");
        CarTwoEngineLevel = PlayerPrefs.GetInt("CarTwoEngineLevel");
        CarThreeEngineLevel = PlayerPrefs.GetInt("CarThreeEngineLevel");
        CarFourEngineLevel = PlayerPrefs.GetInt("CarFourEngineLevel");
        CarFiveEngineLevel = PlayerPrefs.GetInt("CarFiveEngineLevel");
        CarSixEngineLevel = PlayerPrefs.GetInt("CarSixEngineLevel");
        CarSevenEngineLevel = PlayerPrefs.GetInt("CarSevenEngineLevel");

        CarOneSpeed = PlayerPrefs.GetInt("CarOneSpeed");
        CarTwoSpeed = PlayerPrefs.GetInt("CarTwoSpeed");
        CarThreeSpeed = PlayerPrefs.GetInt("CarThreeSpeed");
        CarFourSpeed = PlayerPrefs.GetInt("CarFourSpeed");
        CarFiveSpeed = PlayerPrefs.GetInt("CarFiveSpeed");
        CarSixSpeed = PlayerPrefs.GetInt("CarSixSpeed");
        CarSevenSpeed = PlayerPrefs.GetInt("CarSevenSpeed");


        int EngineUpdateValue = 100;

        if (selectedCar == 0)
        {
            Debug.Log("Car one engine level : " + CarOneEngineLevel);
            if (CarOneEngineLevel < 10)
            {
                CarOneCoinNeedForEngine = PlayerPrefs.GetFloat("CarOneCoinNeedForEngine");
                CarOneEngineUpdatePercentage = CarOneEngineLevel + 10;
                if (coin >= CarOneCoinNeedForEngine)
                {
                    coin = coin - CarOneCoinNeedForEngine;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarOneCoinNeedForEngine = CoinNeed(CarOneCoinNeedForEngine, CarOneEngineUpdatePercentage);
                    PlayerPrefs.SetFloat("CarOneCoinNeedForEngine", CarOneCoinNeedForEngine);
                    if (CarOneEngineLevel == 9)
                    {
                        EngineValue.text = "Max";
                        EngineCoinIcon.SetActive(false);
                    }
                    else
                    {
                        EngineCoinIcon.SetActive(true);
                        EngineValue.text = PlayerPrefs.GetFloat("CarOneCoinNeedForEngine").ToString();
                    }
                    CarOneEngineLevel++;
                    PlayerPrefs.SetInt("CarOneEngineLevel", CarOneEngineLevel);
                    EngineLevel.text = PlayerPrefs.GetInt("CarOneEngineLevel").ToString() + "/10";
                    CarOneSpeed = CarOneSpeed + EngineUpdateValue;
                    PlayerPrefs.SetInt("CarOneSpeed", CarOneSpeed);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarOneEngineCurrentValue: " + CarOneCoinNeedForEngine);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarOneCoinNeedForEngine + "coin");
                }
            }
        }
        if (selectedCar == 1)
        {
            Debug.Log("Car Two engine level : " + CarTwoEngineLevel);
            if (CarTwoEngineLevel < 10)
            {
                CarTwoCoinNeedForEngine = PlayerPrefs.GetFloat("CarTwoCoinNeedForEngine");
                CarTwoEngineUpdatePercentage = CarTwoEngineLevel + 10;
                if (coin >= CarTwoCoinNeedForEngine)
                {
                    coin = coin - CarTwoCoinNeedForEngine;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarTwoCoinNeedForEngine = CoinNeed(CarTwoCoinNeedForEngine, CarTwoEngineUpdatePercentage);
                    PlayerPrefs.SetFloat("CarTwoCoinNeedForEngine", CarTwoCoinNeedForEngine);
                    if (CarTwoEngineLevel == 9)
                    {
                        EngineValue.text = "Max";
                        EngineCoinIcon.SetActive(false);
                    }
                    else
                    {
                        EngineCoinIcon.SetActive(true);
                        EngineValue.text = PlayerPrefs.GetFloat("CarTwoCoinNeedForEngine").ToString();
                    }
                    CarTwoEngineLevel++;
                    PlayerPrefs.SetInt("CarTwoEngineLevel", CarTwoEngineLevel);
                    EngineLevel.text = PlayerPrefs.GetInt("CarTwoEngineLevel").ToString() + "/10";
                    CarTwoSpeed = CarTwoSpeed + EngineUpdateValue;
                    PlayerPrefs.SetInt("CarTwoSpeed", CarTwoSpeed);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarTwoEngineCurrentValue: " + CarTwoCoinNeedForEngine);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarTwoCoinNeedForEngine + "coin");
                }
            }
        }
        if (selectedCar == 2)
        {
            Debug.Log("Car Three engine level : " + CarThreeEngineLevel);
            if (CarThreeEngineLevel < 10)
            {
                CarThreeCoinNeedForEngine = PlayerPrefs.GetFloat("CarThreeCoinNeedForEngine");
                CarThreeEngineUpdatePercentage = CarThreeEngineLevel + 10;
                if (coin >= CarThreeCoinNeedForEngine)
                {
                    coin = coin - CarThreeCoinNeedForEngine;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarThreeCoinNeedForEngine = CoinNeed(CarThreeCoinNeedForEngine, CarThreeEngineUpdatePercentage);
                    PlayerPrefs.SetFloat("CarThreeCoinNeedForEngine", CarThreeCoinNeedForEngine);
                    if (CarThreeEngineLevel == 9)
                    {
                        EngineValue.text = "Max";
                        EngineCoinIcon.SetActive(false);
                    }
                    else
                    {
                        EngineCoinIcon.SetActive(true);
                        EngineValue.text = PlayerPrefs.GetFloat("CarThreeCoinNeedForEngine").ToString();
                    }
                    CarThreeEngineLevel++;
                    PlayerPrefs.SetInt("CarThreeEngineLevel", CarThreeEngineLevel);
                    EngineLevel.text = PlayerPrefs.GetInt("CarThreeEngineLevel").ToString() + "/10";
                    CarThreeSpeed = CarThreeSpeed + EngineUpdateValue;
                    PlayerPrefs.SetInt("CarThreeSpeed", CarThreeSpeed);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarThreeEngineCurrentValue: " + CarThreeCoinNeedForEngine);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarThreeCoinNeedForEngine + "coin");
                }
            }
        }
        if (selectedCar == 3)
        {
            Debug.Log("Car Four engine level : " + CarFourEngineLevel);
            if (CarFourEngineLevel < 10)
            {
                CarFourCoinNeedForEngine = PlayerPrefs.GetFloat("CarFourCoinNeedForEngine");
                CarFourEngineUpdatePercentage = CarFourEngineLevel + 10;
                if (coin >= CarFourCoinNeedForEngine)
                {
                    coin = coin - CarFourCoinNeedForEngine;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarFourCoinNeedForEngine = CoinNeed(CarFourCoinNeedForEngine, CarFourEngineUpdatePercentage);
                    PlayerPrefs.SetFloat("CarFourCoinNeedForEngine", CarFourCoinNeedForEngine);
                    if (CarFourEngineLevel == 9)
                    {
                        EngineValue.text = "Max";
                        EngineCoinIcon.SetActive(false);
                    }
                    else
                    {
                        EngineCoinIcon.SetActive(true);
                        EngineValue.text = PlayerPrefs.GetFloat("CarFourCoinNeedForEngine").ToString();
                    }
                    CarFourEngineLevel++;
                    PlayerPrefs.SetInt("CarFourEngineLevel", CarFourEngineLevel);
                    EngineLevel.text = PlayerPrefs.GetInt("CarFourEngineLevel").ToString() + "/10";
                    CarFourSpeed = CarFourSpeed + EngineUpdateValue;
                    PlayerPrefs.SetInt("CarFourSpeed", CarFourSpeed);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarFourEngineCurrentValue: " + CarFourCoinNeedForEngine);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarFourCoinNeedForEngine + "coin");
                }
            }
        }
        if (selectedCar == 4)
        {
            Debug.Log("Car Five engine level : " + CarFiveEngineLevel);
            if (CarFiveEngineLevel < 10)
            {
                CarFiveCoinNeedForEngine = PlayerPrefs.GetFloat("CarFiveCoinNeedForEngine");
                CarFiveEngineUpdatePercentage = CarFiveEngineLevel + 10;
                if (coin >= CarFiveCoinNeedForEngine)
                {
                    coin = coin - CarFiveCoinNeedForEngine;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarFiveCoinNeedForEngine = CoinNeed(CarFiveCoinNeedForEngine, CarFiveEngineUpdatePercentage);
                    PlayerPrefs.SetFloat("CarFiveCoinNeedForEngine", CarFiveCoinNeedForEngine);
                    if (CarFiveEngineLevel == 9)
                    {
                        EngineValue.text = "Max";
                        EngineCoinIcon.SetActive(false);
                    }
                    else
                    {
                        EngineCoinIcon.SetActive(true);
                        EngineValue.text = PlayerPrefs.GetFloat("CarFiveCoinNeedForEngine").ToString();
                    }
                    CarFiveEngineLevel++;
                    PlayerPrefs.SetInt("CarFiveEngineLevel", CarFiveEngineLevel);
                    EngineLevel.text = PlayerPrefs.GetInt("CarFiveEngineLevel").ToString() + "/10";
                    CarFiveSpeed = CarFiveSpeed + EngineUpdateValue;
                    PlayerPrefs.SetInt("CarFiveSpeed", CarFiveSpeed);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarFiveEngineCurrentValue: " + CarFiveCoinNeedForEngine);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarFiveCoinNeedForEngine + "coin");
                }
            }
        }
        if (selectedCar == 5)
        {
            Debug.Log("Car Six engine level : " + CarSixEngineLevel);
            if (CarSixEngineLevel < 10)
            {
                CarSixCoinNeedForEngine = PlayerPrefs.GetFloat("CarSixCoinNeedForEngine");
                CarSixEngineUpdatePercentage = CarSixEngineLevel + 10;
                if (coin >= CarSixCoinNeedForEngine)
                {
                    coin = coin - CarSixCoinNeedForEngine;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarSixCoinNeedForEngine = CoinNeed(CarSixCoinNeedForEngine, CarSixEngineUpdatePercentage);
                    PlayerPrefs.SetFloat("CarSixCoinNeedForEngine", CarSixCoinNeedForEngine);
                    if (CarSixEngineLevel == 9)
                    {
                        EngineValue.text = "Max";
                        EngineCoinIcon.SetActive(false);
                    }
                    else
                    {
                        EngineCoinIcon.SetActive(true);
                        EngineValue.text = PlayerPrefs.GetFloat("CarSixCoinNeedForEngine").ToString();
                    }
                    CarSixEngineLevel++;
                    PlayerPrefs.SetInt("CarSixEngineLevel", CarSixEngineLevel);
                    EngineLevel.text = PlayerPrefs.GetInt("CarSixEngineLevel").ToString() + "/10";
                    CarSixSpeed = CarSixSpeed + EngineUpdateValue;
                    PlayerPrefs.SetInt("CarSixSpeed", CarSixSpeed);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarSixEngineCurrentValue: " + CarSixCoinNeedForEngine);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarSixCoinNeedForEngine + "coin");
                }
            }
        }
        if (selectedCar == 6)
        {
            Debug.Log("Car Seven engine level : " + CarSevenEngineLevel);
            if (CarSevenEngineLevel < 10)
            {
                CarSevenCoinNeedForEngine = PlayerPrefs.GetFloat("CarSevenCoinNeedForEngine");
                CarSevenEngineUpdatePercentage = CarSevenEngineLevel + 10;
                if (coin >= CarSevenCoinNeedForEngine)
                {
                    coin = coin - CarSevenCoinNeedForEngine;
                    PlayerPrefs.SetFloat("Coin", coin);
                    CarSevenCoinNeedForEngine = CoinNeed(CarSevenCoinNeedForEngine, CarSevenEngineUpdatePercentage);
                    PlayerPrefs.SetFloat("CarSevenCoinNeedForEngine", CarSevenCoinNeedForEngine);
                    if (CarSevenEngineLevel == 9)
                    {
                        EngineValue.text = "Max";
                        EngineCoinIcon.SetActive(false);
                    }
                    else
                    {
                        EngineCoinIcon.SetActive(true);
                        EngineValue.text = PlayerPrefs.GetFloat("CarSevenCoinNeedForEngine").ToString();
                    }
                    CarSevenEngineLevel++;
                    PlayerPrefs.SetInt("CarSevenEngineLevel", CarSevenEngineLevel);
                    EngineLevel.text = PlayerPrefs.GetInt("CarSevenEngineLevel").ToString() + "/10";
                    CarSevenSpeed = CarSevenSpeed + EngineUpdateValue;
                    PlayerPrefs.SetInt("CarSevenSpeed", CarSevenSpeed);
                    Debug.Log("Car " + selectedCar + " has been updated successfully");
                    Debug.Log("CarSevenEngineCurrentValue: " + CarSevenCoinNeedForEngine);
                }
                else
                {
                    PlayerPrefs.SetInt("InsufficientCoin", 1);
                    StartCoroutine("InsufficientCoinTime");
                    Debug.Log("Insufficient coin");
                    Debug.Log("You have to have at least " + CarSevenCoinNeedForEngine + "coin");
                }
            }
        }
    }


    //Calculation of Needed coin for update
    float CoinNeed(float currentValue, int percentage)
    {
        float nextValue = currentValue + (currentValue * 0.1f);
        return Mathf.Round(nextValue);
        //return nextValue;
    }
    IEnumerator InsufficientCoinTime()
    {
        yield return new WaitForSeconds(.3f);
        PlayerPrefs.SetInt("InsufficientCoin", 0);
    }
}
