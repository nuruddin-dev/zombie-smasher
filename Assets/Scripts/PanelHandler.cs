﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelHandler : MonoBehaviour
{
    int selectedPanel = 0;
    // Start is called before the first frame update
    void Start()
    {
        SelectPanel();
    }

    public void NextPanel()
    {
        int previousSelectedPanel = selectedPanel;
        if (selectedPanel >= transform.childCount - 1)
        {
            selectedPanel = 0;
        }
        else selectedPanel++;
        if (previousSelectedPanel != selectedPanel)
        {
            SelectPanel();
        }

        Debug.Log("Panel no " + (selectedPanel + 1) + " is selected");
    }

    public void PreviousPanel()
    {
        int previousSelectedPanel = selectedPanel;
        if (selectedPanel <= 0)
        {
            selectedPanel = transform.childCount - 1;
        }
        else selectedPanel--;
        if (previousSelectedPanel != selectedPanel)
        {
            SelectPanel();
        }
        Debug.Log("Panel no " + (selectedPanel + 1) + " is selected");
    }

    void SelectPanel()
    {
        int i = 0;

        foreach (Transform Panel in transform)
        {
            if (i == selectedPanel)
            {
                Panel.gameObject.SetActive(true);
            }
            else Panel.gameObject.SetActive(false);
            i++;
        }
    }
}
