﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelFinish : MonoBehaviour
{
    public GameObject GearButton, FireButton, ReplayPanel;
    public GameObject UIButtons;
    int currentLevel;

    void Start()
    {
        UIButtons = GameObject.Find("UIButtons");
        ReplayPanel = GameObject.Find("ReplayPanel");
        ReplayPanel.SetActive(false);
        if(GameObject.Find("Ground_1")!=null)
        {
            currentLevel = 1;
        }
        if (GameObject.Find("Ground_2") != null)
        {
            currentLevel = 2;
        }
        if (GameObject.Find("Ground_3") != null)
        {
            currentLevel = 3;
        }
        if (GameObject.Find("Ground_4") != null)
        {
            currentLevel = 4;
        }
        if (GameObject.Find("Ground_5") != null)
        {
            currentLevel = 5;
        }
        if (GameObject.Find("Ground_6") != null)
        {
            currentLevel = 6;
        }
        if (GameObject.Find("Ground_7") != null)
        {
            currentLevel = 7;
        }
        if (GameObject.Find("Ground_8") != null)
        {
            currentLevel = 8;
        }
        if (GameObject.Find("Ground_9") != null)
        {
            currentLevel = 9;
        }
        if (GameObject.Find("Ground_10") != null)
        {
            currentLevel = 10;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.transform.tag == "CarDie")
        {
            //Debug.Log("trigger tag: " + collider.tag);
            Time.timeScale = 0;
            UIButtons.SetActive(false);
            ReplayPanel.SetActive(true);
        }
        if (collider.transform.tag == "Car")
        {
            Debug.Log("trigger tag: " + collider.tag);
            if (collider.GetComponent<CarOne>() != null)
            {
                collider.GetComponent<CarOne>().movement = 0.001f;
                collider.GetComponent<CarOne>().finish = true;
                collider.GetComponent<CarOne>().rb.velocity = Vector2.zero;
            }
            if (collider.GetComponent<CarTwo>() != null)
            {
                collider.GetComponent<CarTwo>().movement = 0.001f;
                collider.GetComponent<CarTwo>().finish = true;
                collider.GetComponent<CarTwo>().rb.velocity = Vector2.zero;
            }
            if (collider.GetComponent<CarThree>() != null)
            {
                collider.GetComponent<CarThree>().movement = 0.001f;
                collider.GetComponent<CarThree>().finish = true;
                collider.GetComponent<CarThree>().rb.velocity = Vector2.zero;
            }
            if (collider.GetComponent<CarFour>() != null)
            {
                collider.GetComponent<CarFour>().movement = 0.001f;
                collider.GetComponent<CarFour>().finish = true;
                collider.GetComponent<CarFour>().rb.velocity = Vector2.zero;
            }
            if (collider.GetComponent<CarFive>() != null)
            {
                collider.GetComponent<CarFive>().movement = 0.001f;
                collider.GetComponent<CarFive>().finish = true;
                collider.GetComponent<CarFive>().rb.velocity = Vector2.zero;
            }
            if (collider.GetComponent<CarSix>() != null)
            {
                collider.GetComponent<CarSix>().movement = 0.001f;
                collider.GetComponent<CarSix>().finish = true;
                collider.GetComponent<CarSix>().rb.velocity = Vector2.zero;
            }
            if (collider.GetComponent<CarSeven>() != null)
            {
                collider.GetComponent<CarSeven>().movement = 0.001f;
                collider.GetComponent<CarSeven>().finish = true;
                collider.GetComponent<CarSeven>().rb.velocity = Vector2.zero;
            }
            //GearButton = GameObject.Find("Gear Button");
            //FireButton = GameObject.Find("Fire Button");
            //GearButton.GetComponent<Button>().interactable = false;
            //FireButton.GetComponent<Button>().interactable = false;
            UIButtons.SetActive(false);
            //StartCoroutine(GotoMenu());
            PlayerPrefs.SetInt("LevelReached", currentLevel + 1);
            ReplayPanel.SetActive(true);
        }
    }

    IEnumerator GotoMenu()
    {
        PlayerPrefs.SetInt("LevelReached", currentLevel+1);
        Debug.Log("level reached: " + PlayerPrefs.GetInt("LevelReached"));
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("Menu");
    }
}
