﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public Transform startPoint, endPoint;
    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = true;
        lineRenderer.useWorldSpace = true;
    }

    // Update is called once per frame
    void Update()
    {
        //RaycastHit2D hit = Physics2D.Raycast(startPoint.position, startPoint.Right);
        //Debug.DrawLine(transform.position, hit.point);
        //endPoint.position = hit.point;
        //laserHit.position = 
        lineRenderer.SetPosition(0, startPoint.position);
        lineRenderer.SetPosition(1, endPoint.position);
        //if (Input.GetKey(KeyCode.Space))
        //{
        //    lineRenderer.enabled = true;
        //}
        //else
        //{
        //    lineRenderer.enabled = false;
        //}
    }
}
