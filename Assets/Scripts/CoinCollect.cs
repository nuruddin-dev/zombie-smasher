﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinCollect : MonoBehaviour
{
    float coin_Y;
    Rigidbody2D rb;
    public GameObject CoinValue;

    // Start is called before the first frame update
    void Start()
    {
        if (transform.position.x > 2900f)
            Destroy(gameObject);
        coin_Y = GameObject.Find("LevelManager").transform.position.y;
        rb = GetComponent<Rigidbody2D>();
        if (rb.isKinematic)
            rb.isKinematic = false;
        rb.velocity = new Vector3(0f, -50f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        if(GameObject.FindWithTag("Car").transform.position.x > transform.position.x +5f)
        {
            Instantiate(gameObject, new Vector2(transform.position.x + 250, coin_Y), transform.rotation);
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Ground")
        {
            if (!rb.isKinematic)
            {
                rb.isKinematic = true;
            }
            else
            {
                rb.isKinematic = false;
            }
            transform.position = new Vector2(transform.position.x, transform.position.y + 2f);
            //gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
        }
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.transform.tag != "Ground")
        {
            FindObjectOfType<AudioManager>().Play("Coin_Pickup");

            float[] value = { 50, 100, 500 };
            Instantiate(gameObject, new Vector2(transform.position.x + 250, coin_Y), transform.rotation);
            Vector2 position = Camera.main.WorldToScreenPoint(transform.position);
            GameObject go = Instantiate(CoinValue, position, transform.rotation);
            float coinValue = value[Random.Range(0, 2)];
            float totalCoin = PlayerPrefs.GetFloat("Coin");
            PlayerPrefs.SetFloat("Coin", totalCoin + coinValue);
            
            GameObject.Find("LevelManager").GetComponent<LevelManager>().coin = GameObject.Find("LevelManager").GetComponent<LevelManager>().coin + (int)coinValue;
            go.GetComponent<Text>().text = "+ " + coinValue;
            go.transform.SetParent(GameObject.Find("Canvas").transform, true);
            go.GetComponent<Rigidbody2D>().gravityScale = -50;
            Destroy(go, 1f);
            //rb.isKinematic = false;
            Destroy(gameObject);
        }
    }
}
