﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public GameObject myCar, CarPoint;
    public GameObject[] Cars;
    int selectedCar;
    GameObject carOne;
    public bool brushFire;
    public GameObject GearButton, FireButton, PausePanel, TotalCoin;
    public GameObject UIButtons;
    public GameObject[] Zombie;
    float j;
    public GameObject currentZombie;
    public bool canBrushFire;
    public int test =0;
    public GameObject Coin;
    public float coin;
    AudioSource GameBackground;
    int soundStatus, musicStatus;
    bool isPaused;
    int i = 1;
    public bool fuelUse;

    void Awake()
    {
        //Prevent screen to time out while game is playing
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        //gameBackgroundAudioStatus audio checking
        if (PlayerPrefs.HasKey("MusicStatus"))
        {
            musicStatus = PlayerPrefs.GetInt("MusicStatus");
        }
        else
        {
            musicStatus = 1;
            PlayerPrefs.SetInt("MusicStatus", 1);
        }


        Time.timeScale = 1;
        //GearButton = GameObject.Find("Gear Button");
        //FireButton = GameObject.Find("Fire Button");
        UIButtons = GameObject.Find("UIButtons");
        PausePanel = GameObject.Find("PausePanel");
        TotalCoin = GameObject.Find("TotalCoin");
        PausePanel.SetActive(false);
        selectedCar = CarManager.selectedCar;
        CarPoint = GameObject.Find("CarPoint");

        //Get all car in Car array
        Cars = new GameObject[7];
        Cars[0] = GameObject.Find("Car_1");
        Cars[1] = GameObject.Find("Car_2");
        Cars[2] = GameObject.Find("Car_3");
        Cars[3] = GameObject.Find("Car_4");
        Cars[4] = GameObject.Find("Car_5");
        Cars[5] = GameObject.Find("Car_6");
        Cars[6] = GameObject.Find("Car_7");

        //This loop active the selected car
        for (int i = 0; i < Cars.Length; i++)
        {
            if (selectedCar == i)
            {
                Cars[i].SetActive(true);
                myCar = Cars[i];
            }
            else
            {
                Cars[i].SetActive(false);
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
        //Game background sound "ON" or "OFF"
        if (musicStatus == 1)
            FindObjectOfType<AudioManager>().MusicPlay("Game_Background");


        canBrushFire = true;
        Coin = GameObject.Find("Coin");

        //Get all zombie in Zombie array
        Zombie = new GameObject[16];
        Zombie[0] = GameObject.Find("F_Zombie_1");
        Zombie[1] = GameObject.Find("F_Zombie_2");
        Zombie[2] = GameObject.Find("F_Zombie_3");
        Zombie[3] = GameObject.Find("F_Zombie_4");
        Zombie[4] = GameObject.Find("F_Zombie_5");
        Zombie[5] = GameObject.Find("F_Zombie_6");
        Zombie[6] = GameObject.Find("F_Zombie_7");
        Zombie[7] = GameObject.Find("G_Zombie_1");
        Zombie[8] = GameObject.Find("G_Zombie_2");
        Zombie[9] = GameObject.Find("G_Zombie_3");
        Zombie[10] = GameObject.Find("G_Zombie_4");
        Zombie[11] = GameObject.Find("G_Zombie_5");
        Zombie[12] = GameObject.Find("G_Zombie_6");
        Zombie[13] = GameObject.Find("G_Zombie_7");
        //Zombie[14] = GameObject.Find("G_Zombie_8");
        Zombie[14] = GameObject.Find("G_Zombie_9");
        Zombie[15] = GameObject.Find("G_Zombie_10");

    }
    void Update()
    {
        Debug.Log("battery power : " + Battery.batteryPower);
        Debug.Log("fuel power : " + Battery.fuelPower);
        if (fuelUse)
        {
            Battery.fuelPower--;
        }
    }
    void FixedUpdate()
    {
        coin = PlayerPrefs.GetFloat("Coin");
        //Debug.Log("is paused: " + isPaused);
        TotalCoin.GetComponent<Text>().text = "" + coin;
        
        if (Input.GetKey(KeyCode.A))
        {
            PlayerPrefs.SetInt("MenuFromLevel", 1);
            Debug.Log("Menu from level is called");
        }

        if (isPaused)
        {
            Pause();
        }
        if (Input.GetKey(KeyCode.Escape))
        {
            if ((i%2)!=0)
            {
                Pause();
            }
            if ((i % 2) == 0)
            {
                Resume();
            }
            i++;
        }

        //if (Input.GetKey(KeyCode.L))
        //{
        //    PlayerPrefs.SetInt("LevelReached", 2);
        //}

        CarPoint.transform.position = myCar.transform.position;

        if (brushFire)
        {
            // continuous fire code here.
        }
    }

    public void Pause()
    {
        Time.timeScale = 0;
        PausePanel.SetActive(true);
        UIButtons.SetActive(false);
        //GearButton.SetActive(false);
        //FireButton.SetActive(false);
    }
    public void Resume()
    {
        Time.timeScale = 1;
        PausePanel.SetActive(false);
        UIButtons.SetActive(true);
        //GearButton.SetActive(true);
        //FireButton.SetActive(true);
    }
    //void OnApplicationFocus(bool hasFocus)
    //{
    //    isPaused =  !hasFocus;
    //}
    public void GotoHome()
    {
        PlayerPrefs.SetInt("MenuFromLevel", 0);
        SceneManager.LoadScene("Menu");
    }
    public void GotoGarage()
    {
        PlayerPrefs.SetInt("MenuFromLevel", 1);
        SceneManager.LoadScene("Menu");
    }
    public void Exit()
    {
        Application.Quit();
    }

    //This function will be called when Gear button pressed down
    public void GoForward()
    {
        fuelUse = true;
        if (Battery.fuelPower > 0)
        {
            if (selectedCar == 0)
            {
                if (myCar.GetComponent<CarOne>() != null)
                {
                    myCar.GetComponent<CarOne>().GoForward();
                }
            }
            if (selectedCar == 1)
            {
                if (myCar.GetComponent<CarTwo>() != null)
                {
                    myCar.GetComponent<CarTwo>().GoForward();
                }
            }
            if (selectedCar == 2)
            {
                if (myCar.GetComponent<CarThree>() != null)
                {
                    myCar.GetComponent<CarThree>().GoForward();
                }
            }
            if (selectedCar == 3)
            {
                if (myCar.GetComponent<CarFour>() != null)
                {
                    myCar.GetComponent<CarFour>().GoForward();
                }
            }
            if (selectedCar == 4)
            {
                if (myCar.GetComponent<CarFive>() != null)
                {
                    myCar.GetComponent<CarFive>().GoForward();
                }
            }
            if (selectedCar == 5)
            {
                if (myCar.GetComponent<CarSix>() != null)
                {
                    myCar.GetComponent<CarSix>().GoForward();
                }
            }
            if (selectedCar == 6)
            {
                if (myCar.GetComponent<CarSeven>() != null)
                {
                    myCar.GetComponent<CarSeven>().GoForward();
                }
            }
        }
    }

    //This function will be called when Gear button released
    public void SlowDown()
    {
        fuelUse = false;
        if (selectedCar == 0)
        {
            if (myCar.GetComponent<CarOne>() != null)
            {
                myCar.GetComponent<CarOne>().SlowDown();
            }
        }
        if (selectedCar == 1)
        {
            if (myCar.GetComponent<CarTwo>() != null)
            {
                myCar.GetComponent<CarTwo>().SlowDown();
            }
        }
        if (selectedCar == 2)
        {
            if (myCar.GetComponent<CarThree>() != null)
            {
                myCar.GetComponent<CarThree>().SlowDown();
            }
        }
        if (selectedCar == 3)
        {
            if (myCar.GetComponent<CarFour>() != null)
            {
                myCar.GetComponent<CarFour>().SlowDown();
            }
        }
        if (selectedCar == 4)
        {
            if (myCar.GetComponent<CarFive>() != null)
            {
                myCar.GetComponent<CarFive>().SlowDown();
            }
        }
        if (selectedCar == 5)
        {
            if (myCar.GetComponent<CarSix>() != null)
            {
                myCar.GetComponent<CarSix>().SlowDown();
            }
        }
        if (selectedCar == 6)
        {
            if (myCar.GetComponent<CarSeven>() != null)
            {
                myCar.GetComponent<CarSeven>().SlowDown();
            }
        }
    }


    //This function will be called when fire button pressed down.
    public void BrushFire()
    {
            if (myCar.GetComponent<CarTwo>() != null)
            {
                Debug.Log("CarTwo brush fire is called");
                myCar.GetComponent<CarTwo>().weapon = true;
                //StartCoroutine(WaitForCarTwoBrushFire());
            }
            if (myCar.GetComponent<CarFour>() != null)
            {
                Debug.Log("CarFour brush fire is called");
                myCar.GetComponent<CarFour>().weapon = true;
                //StartCoroutine(WaitForCarFourBrushFire());
            }
            if (myCar.GetComponent<CarFive>() != null)
            {
                Debug.Log("CarFive brush fire is called");
                myCar.GetComponent<CarFive>().weapon = true;
                //StartCoroutine(WaitForCarFiveBrushFire());
            }
            if (myCar.GetComponent<CarSix>() != null)
            {
                Debug.Log("CarSix brush fire is called");
                myCar.GetComponent<CarSix>().weapon = true;
                //StartCoroutine(WaitForCarSixBrushFire());
            }
            if (myCar.GetComponent<CarSeven>() != null)
            {
                Debug.Log("CarSeven brush fire is called");
                myCar.GetComponent<CarSeven>().weapon = true;
                //StartCoroutine(WaitForCarSevenBrushFire());
            }

            //StartCoroutine(WaitForNextBrushFire());
        
    }

    //This function will be called when fire button pressed up.
    public void BrushFireStop()
    {
        if (myCar.GetComponent<CarTwo>() != null)
        {
            Debug.Log("CarTwo brush fire is called");
            myCar.GetComponent<CarTwo>().weapon = false;
        }
        if (myCar.GetComponent<CarFour>() != null)
        {
            Debug.Log("CarFour brush fire is called");
            myCar.GetComponent<CarFour>().weapon = false;
        }
        if (myCar.GetComponent<CarFive>() != null)
        {
            Debug.Log("CarFive brush fire is called");
            myCar.GetComponent<CarFive>().weapon = false;
        }
        if (myCar.GetComponent<CarSix>() != null)
        {
            Debug.Log("CarSix brush fire is called");
            myCar.GetComponent<CarSix>().weapon = false;
        }
        if (myCar.GetComponent<CarSeven>() != null)
        {
            Debug.Log("CarSeven brush fire is called");
            myCar.GetComponent<CarSeven>().weapon = false;
        }
    }

    //This function will be called when fire button clicked.
    public void Fire()
    {
        if (myCar.GetComponent<CarOne>() != null)
        {
            myCar.GetComponent<CarOne>().weapon = true;
        }
        StartCoroutine(WaitForNextFire());
    }
    IEnumerator WaitForNextFire()
    {
        yield return new WaitForSeconds(.01f);
        if (myCar.GetComponent<CarOne>() != null)
        {
            myCar.GetComponent<CarOne>().weapon = false;
        }
    }

    public void Shield()
    {
        if (Battery.fuelPower > 0)
        {
            if (myCar.GetComponent<CarOne>() != null)
            {
                myCar.GetComponent<CarOne>().shield = true;
            }
            if (myCar.GetComponent<CarTwo>() != null)
            {
                myCar.GetComponent<CarTwo>().shield = true;
            }
            if (myCar.GetComponent<CarThree>() != null)
            {
                myCar.GetComponent<CarThree>().shield = true;
            }
            if (myCar.GetComponent<CarFour>() != null)
            {
                myCar.GetComponent<CarFour>().shield = true;
            }
            if (myCar.GetComponent<CarFive>() != null)
            {
                myCar.GetComponent<CarFive>().shield = true;
            }
            if (myCar.GetComponent<CarSix>() != null)
            {
                myCar.GetComponent<CarSix>().shield = true;
            }
            if (myCar.GetComponent<CarSeven>() != null)
            {
                myCar.GetComponent<CarSeven>().shield = true;
            }
        }
        if (Battery.batteryPower < 0)
            Battery.batteryPower = 0;
        //StartCoroutine(WaitForNextBrushFire());

    }

    public void ShieldStop()
    {
        if (myCar.GetComponent<CarOne>() != null)
        {
            myCar.GetComponent<CarOne>().shield = false;
        }
        if (myCar.GetComponent<CarTwo>() != null)
        {
            myCar.GetComponent<CarTwo>().shield = false;
        }
        if (myCar.GetComponent<CarThree>() != null)
        {
            myCar.GetComponent<CarThree>().shield = false;
        }
        if (myCar.GetComponent<CarFour>() != null)
        {
            myCar.GetComponent<CarFour>().shield = false;
        }
        if (myCar.GetComponent<CarFive>() != null)
        {
            myCar.GetComponent<CarFive>().shield = false;
        }
        if (myCar.GetComponent<CarSix>() != null)
        {
            myCar.GetComponent<CarSix>().shield = false;
        }
        if (myCar.GetComponent<CarSeven>() != null)
        {
            myCar.GetComponent<CarSeven>().shield = false;
        }
    }

    //IEnumerator WaitForCarTwoBrushFire()
    //{
    //    yield return new WaitForSeconds(.5f);
    //    myCar.GetComponent<CarTwo>().weapon = true;
    //    yield return new WaitForSeconds(2f);
    //    myCar.GetComponent<CarTwo>().weapon = false;
    //}
    //IEnumerator WaitForCarFourBrushFire()
    //{
    //    yield return new WaitForSeconds(.5f);
    //    myCar.GetComponent<CarFour>().weapon = true;
    //    yield return new WaitForSeconds(2f);
    //    myCar.GetComponent<CarFour>().weapon = false;
    //}
    //IEnumerator WaitForCarFiveBrushFire()
    //{
    //    yield return new WaitForSeconds(.5f);
    //    myCar.GetComponent<CarFive>().weapon = true;
    //    yield return new WaitForSeconds(2f);
    //    myCar.GetComponent<CarFive>().weapon = false;
    //}
    //IEnumerator WaitForCarSixBrushFire()
    //{
    //    yield return new WaitForSeconds(.5f);
    //    myCar.GetComponent<CarSix>().weapon = true;
    //    yield return new WaitForSeconds(2f);
    //    myCar.GetComponent<CarSix>().weapon = false;
    //}
    //IEnumerator WaitForCarSevenBrushFire()
    //{
    //    yield return new WaitForSeconds(.5f);
    //    myCar.GetComponent<CarSeven>().weapon = true;
    //    yield return new WaitForSeconds(2f);
    //    myCar.GetComponent<CarSeven>().weapon = false;
    //}

    public void Restart(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}
