﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    public Animator Zombianimators;
    public GameObject myCars;
    public float ZombieLife = 100f;
    public GameObject zombieBody, zombieHead, zombieHandLeft, zombieHandRight, 
                      zombieLegRight, zombieLegLeft;

    public ParticleSystem[] BloodPartical;

    public Rigidbody2D mycarRigidbody, selfRigidbody, zombieHeadRigidbody, 
                        zombieHandLeftRigidbody, zombieHandRightRigidbody, zombieLegRightRigidbody, 
                         zombieLegLeftRigidbody , zombieBodyRigidbody;

    public bool displace = false;
    public bool soundflag = false;
    public bool inrange = false;
    public float attackdistance = 2f , zombipkosition,carposition,gholder;
    public float zombieDeathTime;
    public float speed = 10f;
    public GameObject[] zombie;
    public int zombieLife = 100;


    // Start is called before the first frame update
    
    void Start()
    {
        Zombianimators = GetComponent<Animator>();

        selfRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        myCars = GameObject.FindGameObjectWithTag("Car");
        mycarRigidbody = myCars.GetComponent<Rigidbody2D>();

        //float distance = Vector2.Distance(myCars.transform.position, transform.position);
        //Debug.Log("distance : " + distance);
        //if (distance == 10f)
        //{
        //    Instantiate(LevelManager.Zombie[Random.Range(0, 15)], new Vector2(transform.position.x + 1000f, GameObject.Find("LevelManager").transform.position.y), transform.rotation);
         
        //}

        if (myCars.transform.position.x > (transform.position.x + 50f))
        {
            Instantiate(transform.gameObject, new Vector2(transform.position.x + 1000f, GameObject.Find("LevelManager").transform.position.y), transform.rotation);
            Destroy(gameObject);
        }

        //Untill zomibe collide with car
        if (displace == false) 
        {
            zombipkosition = transform.position.x;
            carposition = myCars.transform.position.x;

            gholder = Mathf.Abs(zombipkosition - carposition);
            
            Zombianimators.SetFloat("attackrange", gholder);

            if (gholder < 50) {
                inrange = true;
                attackdistance = gholder;
            
            }
            if (inrange) {

                transform.position = new Vector3(transform.position.x - Time.deltaTime * 10, transform.position.y, transform.position.z);

            }
        }

        //when zombie collide with car
        if (displace) 
        {
            gameObject.layer = 8;
            //FindObjectOfType<AudioManager>().Play("Bone_Crack");

            Zombianimators.SetFloat("attackrange", 500f);
            
            if(zombieBody.GetComponent<Rigidbody2D>() == null)
                zombieBody.AddComponent<Rigidbody2D>();
            zombieBodyRigidbody = zombieBody.GetComponent<Rigidbody2D>();

            if (zombieHead.GetComponent<Rigidbody2D>() == null)
                zombieHead.AddComponent<Rigidbody2D>();
            zombieHeadRigidbody = zombieHead.GetComponent<Rigidbody2D>();

            if (zombieHandLeft.GetComponent<Rigidbody2D>() == null)
                zombieHandLeft.AddComponent<Rigidbody2D>();
            zombieHandLeftRigidbody = zombieHandLeft.GetComponent<Rigidbody2D>();

            if (zombieHandRight.GetComponent<Rigidbody2D>() == null)
                zombieHandRight.AddComponent<Rigidbody2D>();
            zombieHandRightRigidbody = zombieHandRight.GetComponent<Rigidbody2D>();

            if (zombieLegRight.GetComponent<Rigidbody2D>() == null)
                zombieLegRight.AddComponent<Rigidbody2D>();
            zombieLegRightRigidbody = zombieLegRight.GetComponent<Rigidbody2D>();

            if (zombieLegLeft.GetComponent<Rigidbody2D>() == null)
                zombieLegLeft.AddComponent<Rigidbody2D>();
            zombieLegLeftRigidbody = zombieLegLeft.GetComponent<Rigidbody2D>();


            

            //self
            selfRigidbody.velocity = new Vector3(10f, 100f, 0f);
            //body
            zombieBodyRigidbody.velocity = new Vector3(20f, 0f, 0f);
            //head
            zombieHeadRigidbody.velocity = new Vector3(15f, 3f, 0f);
            zombieHeadRigidbody.rotation = 2f;
            //left hnd
            zombieHandLeftRigidbody.velocity = new Vector3(10f, 2f, 2f);
            zombieHandLeftRigidbody.rotation = 1f;
            //right hnd
            zombieHandRightRigidbody.velocity = new Vector3(10f, -1f, 0f);
            zombieHandRightRigidbody.rotation = 1f;
            //right leg
            zombieLegRightRigidbody.velocity = new Vector3(10f, 2f, 2f);
            zombieLegRightRigidbody.rotation = 1f;
            //left leg
            zombieLegLeftRigidbody.velocity = new Vector3(10f, -1f, 0f);
            zombieLegLeftRigidbody.rotation = 1f;
            



            BloodPartical[0].Play();
            BloodPartical[1].Play();
            BloodPartical[2].Play();
            BloodPartical[3].Play();
            BloodPartical[4].Play();

            //Destroy(gameObject, 2f);
            zombieLife--;
            //gameObject.SetActive(false);
        }

        if (zombieLife == 0)
        {
            Destroy(gameObject);
        }

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == "Car")
        {
            FindObjectOfType<AudioManager>().Play("G_Zombie_Die");
            if(transform.gameObject != null)

                //Instantiate(GameObject.Find("LevelManager").GetComponent<LevelManager>().Zombie[Random.Range(0, 15)], new Vector2(transform.position.x + 1000f, GameObject.Find("LevelManager").transform.position.y), transform.rotation);
                Instantiate(transform.gameObject, new Vector2(transform.position.x + 1000f, GameObject.Find("LevelManager").transform.position.y), transform.rotation);
        }
           
    }
    //private void OnTriggerStay2D(Collider2D collider)
    //{
    //    if (collider.transform.tag == "Car")
    //    {
    //        Zombianimators.SetFloat("attackrange", 30f);
    //        carSpriteRenderer = myCars.GetComponent<SpriteRenderer>();
    //        CarSpriteColorRed = carSpriteRenderer.color.r;
    //        CarSpriteColorGreen = carSpriteRenderer.color.g;
    //        CarSpriteColorBlue = carSpriteRenderer.color.b;
    //        StartCoroutine(CarHit());
    //    }
    //    if (collider.transform.tag == "Weapon") 
    //    {
    //        displace = true;
    //        zombieDeathTime = 1f;
    //    }
    //}
    void OnCollisionEnter2D(Collision2D collision)
    {
        //System.Type colType =  collision.GetType();
        //if (colType = ParticleSystem)
        //{
        //    Debug.Log("particle system found");
        //}
        if (collision.transform.tag == "Car")
        {
            //if (!soundflag)
            //{
            //    FindObjectOfType<AudioManager>().Play("G_Zombie_Die");
            //    soundflag = true;
            //}
            displace = true;
            //Debug.Log("how many times on collision called " + displace);
        }
        if (collision.transform.tag == "Weapon")
        {
            displace = true;
        }
    }


    IEnumerator CarHit() {
        yield return new WaitForSeconds(0.5f);
        //carSpriteRenderer.color = new Color(100f, 0f, 0f);
        //yield return new WaitForSeconds(0.2f);
        //carSpriteRenderer.color = new Color(CarSpriteColorRed, CarSpriteColorGreen, CarSpriteColorBlue);
    }

    
}
