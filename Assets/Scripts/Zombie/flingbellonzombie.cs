﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flingbellonzombie : MonoBehaviour
{
    public GameObject myCar, bellon, body;
    public float attackTimer, flytime,distance;

    Rigidbody2D zombieRigitbody;
    private bool isattack = true;
    public GameObject zombieBody, zombieHead, zombieHandLeft, zombieHandRight, zombieLegRight, zombieLegLeft, zombiEgg;

    public ParticleSystem[] BloodPartical;

    public Rigidbody2D mycarRigidbody, selfRigidbody, zombieHeadRigidbody,
                        zombieHandLeftRigidbody, zombieHandRightRigidbody, zombieLegRightRigidbody,
                         zombieLegLeftRigidbody, zombieBodyRigidbody;

    bool displace = false;
    public Animator selfAnimator;
    public int soundStatus;

    private void Awake()
    {
        zombieRigitbody = GetComponent<Rigidbody2D>();
        zombieRigitbody.gravityScale = 0f;
    }
    // Start is called before the first frame update
    void Start()
    {
        selfAnimator = GetComponent<Animator>();
        soundStatus = PlayerPrefs.GetInt("SoundStaus");
    }
    // Update is called once per frame
    void Update()
    {
        myCar = GameObject.FindGameObjectWithTag("Car");
        distance =Mathf.Abs( transform.position.x - myCar.transform.position.x );
        selfAnimator.SetFloat("inattackrange", distance);
        if (distance <20 && isattack)
        {
            //problem!
            transform.position = Vector3.Lerp(transform.position, new Vector3(myCar.transform.position.x + 3f, myCar.transform.position.y + 3f, myCar.transform.position.z), Time.deltaTime * 2f);
            if (attackTimer > 5f)
            {
                
                isattack = false;
                attackTimer = 0;
            }
            //StartCoroutine("wait");
        }
        else if (!isattack && distance < 3)
        {
            GameObject zombieEggCreated = Instantiate(zombiEgg, new Vector3(transform.position.x + 3f, transform.position.y - 2f, transform.position.z), transform.rotation);
            zombieEggCreated.GetComponent<Rigidbody2D>().velocity = new Vector3(10f, -20f, 0f);
            isattack = true;
        }
        attackTimer += Time.deltaTime;

        if (displace)
        {//adding displace force
            if(zombieBody.GetComponent<Rigidbody2D>() == null)
                zombieBody.AddComponent<Rigidbody2D>();
                zombieBodyRigidbody = zombieBody.GetComponent<Rigidbody2D>();

            if (zombieHead.GetComponent<Rigidbody2D>() == null)
                zombieHead.AddComponent<Rigidbody2D>();
                zombieHeadRigidbody = zombieHead.GetComponent<Rigidbody2D>();

            if (zombieHandLeft.GetComponent<Rigidbody2D>() == null)
                zombieHandLeft.AddComponent<Rigidbody2D>();
                zombieHandLeftRigidbody = zombieHandLeft.GetComponent<Rigidbody2D>();

            if (zombieHandRight.GetComponent<Rigidbody2D>() == null)
                zombieHandRight.AddComponent<Rigidbody2D>();
                zombieHandRightRigidbody = zombieHandRight.GetComponent<Rigidbody2D>();

            if (zombieLegRight.GetComponent<Rigidbody2D>() == null)
                zombieLegRight.AddComponent<Rigidbody2D>();
                zombieLegRightRigidbody = zombieLegRight.GetComponent<Rigidbody2D>();

            if (zombieLegLeft.GetComponent<Rigidbody2D>() == null)
                zombieLegLeft.AddComponent<Rigidbody2D>();
                zombieLegLeftRigidbody = zombieLegLeft.GetComponent<Rigidbody2D>();

            //selfRigidbody.velocity = new Vector3(100f, 3f, 0f);

            //adding velocity

            zombieHandRightRigidbody.velocity = new Vector3(100f, 3f, 0f);
            zombieBodyRigidbody.velocity = new Vector3(100f, 3f, 0f);
            zombieHeadRigidbody.velocity = new Vector3(100f, 3f, 0f);
            zombieHandLeftRigidbody.velocity = new Vector3(100f, 3f, 0f);
            zombieLegRightRigidbody.velocity = new Vector3(100f, 3f, 0f);
            zombieLegLeftRigidbody.velocity = new Vector3(100f, 3f, 0f);


            //blood 
            BloodPartical[0].Play();
            BloodPartical[1].Play();
            BloodPartical[2].Play();
            BloodPartical[3].Play();
            BloodPartical[4].Play();

            Destroy(gameObject, 1f);
        }
    }
    IEnumerator wait()
    {
        yield return new WaitForSeconds(1.1f);
        isattack = true;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "missile" || collision.gameObject.tag == "Car") {
            if (soundStatus == 1)
                FindObjectOfType<AudioManager>().Play("F_Zombie_Die");
            displace = true; 
            zombieRigitbody.gravityScale = 1f;
            Destroy(gameObject,1f);
            
        }
    }
}
