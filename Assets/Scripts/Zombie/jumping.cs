﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumping : MonoBehaviour
{
    public GameObject myCar,zombiEgg;
    public float carPoistion, distance;
    public Rigidbody2D selfRigitbody;
    public Animator animator;
    public bool isground = false, youCanjump = false, canflip = true,canlayed = false;
    public GameObject destroyPartical;
    public int counter = 0;
    public int soundStatus;

    // Start is called before the first frame update
    void Start()
    {
        selfRigitbody = GetComponent<Rigidbody2D>();
        soundStatus = PlayerPrefs.GetInt("SoundStatus");
        animator = GetComponent<Animator>();
        canflip = false;
    }
    // Update is called once per frame
    void Update()
    {
        
        //find car
        myCar = GameObject.FindGameObjectWithTag("Car");
        // take car position and find distance 
        carPoistion = myCar.transform.position.x;
        distance = Mathf.Abs(transform.position.x - carPoistion);
        //Debug.Log(carPoistion);
        //Debug.Log(distance);
        //animation
        animator.SetFloat("inrange",distance);
        animator.SetBool("isgrounded",isground);
        animator.SetBool("youcanjump", youCanjump);
        if (myCar.transform.position.x > (transform.position.x + 50))
            Destroy(gameObject, 3f);
        //jump
        if (isground && distance <= 40 && !canflip)
        {
            StartCoroutine("canJumpforward");
        }
        else if (isground && distance <= 30 && canflip)
        {
            StartCoroutine("canJumpback");
        }
        if (distance <= .2) {
            canlayed = true;
        }
        if (counter == 0 && distance<= 1 ) {
            counter = 1;
            GameObject zombieEggCreated = Instantiate(zombiEgg, new Vector3(transform.position.x + 1f, transform.position.y - 1f, transform.position.z), transform.rotation);
            zombieEggCreated.GetComponent<Rigidbody2D>().velocity = new Vector3(0f, -50f, 0f);
            canlayed = false;
        }
        //// rotate 
        //if (!canflip && carPoistion > transform.position.x && isground)
        //{
        //    transform.Rotate(0f, 180f, 0f);
        //    canflip = true;
        //}
        //if(canflip && carPoistion < transform.position.x && isground)
        //{
        //    transform.Rotate(0f, 180f, 0f);
        //    canflip = false;
        //}
    }
    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.tag == "ground") {
    //        isground = true;
    //    }
    //}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Car")
        {
            isground = true;
            youCanjump = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Car")
        {
            isground = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Car" || collision.gameObject.tag == "missile")
        {
            if (soundStatus == 1)
                FindObjectOfType<AudioManager>().Play("F_Zombie_Die");

            Destroy(gameObject,.1f);
            GameObject boom = Instantiate(destroyPartical, transform.position, transform.rotation);
            Destroy(boom, 1f);
        }
    }
    private IEnumerator canJumpforward() {
        yield return new WaitForSeconds(.45f);
        selfRigitbody.velocity = new Vector3(-5f, 10f, 0f);
    }
    private IEnumerator canJumpback()
    {
        yield return new WaitForSeconds(.45f);
        selfRigitbody.velocity = new Vector3(5f, 10f, 0f);
    }
}      
