﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlingZomby : MonoBehaviour
{
    public GameObject myCar,destroyPartical;//partical system
    public SpriteRenderer selfsprite;
    public float carPositionX, distance,colorR,colorG,colorB;
    public Transform fristposition;
    public float attackTimer = 0f;
    //bool attackcounter = false;
    public bool isattack = false;
    public int soundStatus;
    public AudioSource dethSound;
    
    // Start is called before the first frame update
    void Start()
    {
        fristposition = GetComponent<Transform>();
        soundStatus = PlayerPrefs.GetInt("SoundStatus");
        dethSound = GetComponent<AudioSource>();
        //bonCrack = GetComponent<AudioSource>();
        //ac = bonCrack.GetComponent<AudioClip>();

        selfsprite = GetComponent<SpriteRenderer>();
        colorG = selfsprite.color.g;
        colorR = selfsprite.color.r;
        colorB = selfsprite.color.b;
    }

    // Update is called once per frame
    void Update()
    {
        myCar = GameObject.FindGameObjectWithTag("Car");
        carPositionX = myCar.transform.position.x;
        distance = Mathf.Abs(transform.position.x - carPositionX);

        if (distance < 50 && !isattack) {
            //problem!
            transform.position = Vector3.Lerp(transform.position,new Vector3( myCar.transform.position.x+30f, myCar.transform.position.y+17f, myCar.transform.position.z), Time.deltaTime * 30f );
            
            if (attackTimer > 3f)
            {      
               attackTimer = 0;
               isattack = true;
            }
            attackTimer += Time.deltaTime;
            //StartCoroutine("wait");
        }
        else if(isattack)
        {
            transform.position = Vector3.Lerp(transform.position, myCar.transform.position , Time.deltaTime * 3f);
            
        }
        
    }
    //IEnumerator wait() {
    //    yield return new WaitForSeconds(.5f);
    //    //isattack = true;
    //}
    //IEnumerator WaitNeno()
    //{
    //    yield return new WaitForSeconds(0.1f);
    //}
    ////destroy
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if (collision.gameObject.tag == "Car")
        //{

        //    if (dethSound.isPlaying)
        //    {

        //        dethSound.Play();

        //    }
        //}
        //if (collision.gameObject.tag == "Car" )
        //{
        //    selfsprite.color = new Color(255f, 0f, 0f);
        //    GameObject boom = Instantiate(destroyPartical, transform.position, transform.rotation);
        //    Destroy(boom, 1f);
        //    Destroy(gameObject);
        //}
        //if (collision.gameObject.tag == "Weapon")
        //{
        //    Destroy(gameObject);
        //    GameObject boom = Instantiate(destroyPartical, transform.position, transform.rotation);
        //    Destroy(boom, 1f);
        //    selfsprite.color = new Color(255f,0f,0f);
        //    StartCoroutine("WaitNeno");
        //    //selfsprite.color = new Color(colorR, colorG, colorB);
        //}
    }
     

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Car")
        {
            if(soundStatus == 1)
                FindObjectOfType<AudioManager>().Play("F_Zombie_Die");
            

            selfsprite.color = new Color(255f, 0f, 0f);
            GameObject boom = Instantiate(destroyPartical, transform.position, transform.rotation);
            Destroy(boom, 1f);
            Destroy(gameObject,.1f);

        }
        if (collider.gameObject.tag == "Weapon")
        {
            if (soundStatus == 1)
                FindObjectOfType<AudioManager>().Play("F_Zombie_Die");

            Destroy(gameObject);
            GameObject boom = Instantiate(destroyPartical, transform.position, transform.rotation);
            Destroy(boom, 1f);
            selfsprite.color = new Color(255f, 0f, 0f);
            //StartCoroutine("WaitNeno");
            //selfsprite.color = new Color(colorR, colorG, colorB);
        }

        //if (collider.tag == "Car")
        //{
        //    //if (transform.gameObject != null)
        //    //    //Instantiate(GameObject.Find("LevelManager").GetComponent<LevelManager>().Zombie[Random.Range(0, 15)], new Vector2(transform.position.x + 1000f, GameObject.Find("LevelManager").transform.position.y), transform.rotation);
        //    //   Instantiate(transform.gameObject, new Vector2(transform.position.x + 1000f, GameObject.Find("LevelManager").transform.position.y), transform.rotation);
        //}
    }
}
