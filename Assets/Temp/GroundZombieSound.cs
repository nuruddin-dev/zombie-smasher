﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundZombieSound : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Car")
        {
            FindObjectOfType<AudioManager>().Play("G_Zombie_Die");
        }
    }
}
