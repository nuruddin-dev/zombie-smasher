﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryOld : MonoBehaviour
{
    public static int batteryPower;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Car")
        {
            batteryPower = 100;
            Instantiate(transform, new Vector2(transform.position.x +50f, transform.position.y), transform.rotation);
            Destroy(gameObject);
        }
    }
}
