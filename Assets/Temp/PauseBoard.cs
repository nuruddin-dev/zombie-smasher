﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseBoard : MonoBehaviour
{
    bool resume, restart, home, garage;
    string levelName;
    // Start is called before the first frame update
    void Start()
    {
        resume = true;
        restart = true;
        home = true;
        garage = true;
        transform.position = new Vector2(transform.position.x, transform.parent.position.y *3f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Screen.height < 720)
        {
            if (transform.position.y > transform.parent.position.y + 150f && resume && restart && home && garage)
            {
                transform.position = new Vector2(transform.position.x, transform.position.y - 50f);
            }
            if (!resume)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    resume = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().Resume();
                }
            }
            if (!restart)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    restart = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().Restart(levelName);
                }
            }
            if (!home)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    home = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().GotoHome();
                }
            }
            if (!garage)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    garage = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().GotoGarage();
                }
            }
        }
        if (Screen.height >= 720 && Screen.height < 1080)
        {
            if (transform.position.y > transform.parent.position.y + 150f && resume && restart && home && garage)
            {
                transform.position = new Vector2(transform.position.x, transform.position.y - 50f);
            }
            if (!resume)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    resume = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().Resume();
                }
            }
            if (!restart)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    restart = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().Restart(levelName);
                }
            }
            if (!home)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    home = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().GotoHome();
                }
            }
            if (!garage)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    garage = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().GotoGarage();
                }
            }
        }
        else
        {
            if (transform.position.y > transform.parent.position.y + 300f && resume && restart && home && garage)
            {
                transform.position = new Vector2(transform.position.x, transform.position.y - 50f);
            }
            if (!resume)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    resume = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().Resume();
                }
            }
            if (!restart)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    restart = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().Restart(levelName);
                }
            }
            if (!home)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    home = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().GotoHome();
                }
            }
            if (!garage)
            {
                if (transform.position.y < transform.parent.position.y * 3f)
                {
                    transform.position = new Vector2(transform.position.x, transform.position.y + 50f);
                }
                else
                {
                    garage = true;
                    GameObject.Find("LevelManager").GetComponent<LevelManager>().GotoGarage();
                }
            }
        }
        

    }

    //Called when RESUME button clicked
    public void Resume()
    {
        resume = false;
    }

    //Called when RESTART button clicked
    public void Restart(string level)
    {
        levelName = level;
        restart = false;
    }

    //Called when HOME button clicked
    public void Home()
    {
        home = false;
    }

    //Called when GARAGE button clicked
    public void Garage()
    {
        garage = false;
    }
}
