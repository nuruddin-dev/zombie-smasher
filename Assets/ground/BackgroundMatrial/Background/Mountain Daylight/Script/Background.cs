﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    public Renderer meshrender;
    public float movespeed;
    public Camera mainCamera;
    public GameObject myCar;

    // Start is called before the first frame update
    void Start()
    {
        meshrender = GetComponent<Renderer>();

        mainCamera = Camera.main;
        myCar = GameObject.FindGameObjectWithTag("Car");
    }
    
    private void LateUpdate()
    {

        meshrender.material.mainTextureOffset = new Vector2(mainCamera.transform.position.x/500, 0f);
        
    }
    
}
