﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LockLevel : MonoBehaviour
{
    public GameObject[] LockIcon;
    public Button[] LevelButton;
    public bool isButtonInteractable;
    // Start is called before the first frame update
    void Start()
    {
        int levelReached = PlayerPrefs.GetInt("LevelReached", 1);
        Debug.Log("level reached: " + levelReached);

        LevelButton = GetComponentsInChildren<Button>();
        for (int i = 0; i < LevelButton.Length; i++)
        {
            if (i >= levelReached)
            {
                LevelButton[i].interactable = false;
            }
        }


        LockIcon = new GameObject[50];
        Debug.Log("lock icon size: " + LockIcon.Length);
        //LevelButton = GetComponentsInChildren<Button>();
        
        for (int i = 0; i < 10; i++)
        {
            LockIcon[i] = LevelButton[i].transform.GetChild(1).gameObject;
            if (!LevelButton[i].interactable)
            {
                LockIcon[i].SetActive(true);
            }
            else
            {
                LockIcon[i].SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySelectedLevel(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
