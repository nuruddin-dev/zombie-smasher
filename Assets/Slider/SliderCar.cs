﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderCar : MonoBehaviour
{

    public float sliderCarPos, flagPos, myCarPos, groundLastPoint, res, myCarlength, sliderCarlength;

    public GameObject sliderCar, flag, myCar, goal;

    // Start is called before the first frame update
    void Start()
    {

        sliderCar = null;
        flag = null;
        myCar = null;
        goal = null;


    }

    // Update is called once per frame
    void Update()
    {
        if (myCar == null) 
        {
            myCar = GameObject.FindGameObjectWithTag("Car");
            goal = GameObject.FindGameObjectWithTag("Goal");

            myCarPos = myCar.transform.position.x;
            groundLastPoint = goal.transform.position.x;



        }
        if (sliderCar == null)
        {
            sliderCar = GameObject.FindGameObjectWithTag("Slidercar");
            flag = GameObject.FindGameObjectWithTag("flag");

            sliderCarPos = sliderCar.transform.position.x;
            flagPos = flag.transform.position.x;
            
        }
        //take all position;

        if (myCar != null && sliderCar != null) {
            myCarlength = groundLastPoint - myCarPos;
            sliderCarlength = flagPos - sliderCarPos;
            res = sliderCarlength / myCarlength;
        }
        sliderCar.transform.position = new Vector3( (transform.position.x + (myCar.transform.position.x * res)) - 30f, sliderCar.transform.position.y, sliderCar.transform.position.z);




    }
}
