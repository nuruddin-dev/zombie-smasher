﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : MonoBehaviour
{
    public GameObject batteryBar, fuleBar;
    SpriteRenderer selfSpriteBattery, selfSpriteFuel;
    Rigidbody2D selfRigidbody;
    CapsuleCollider2D selfColider;
    public static int batteryPower;
    public static int fuelPower;

    float alpha, red, green, blue, BatterycounterChecker = 1, FulecounterChecker = 1;// counterChecker is for EnergyOrFuleBar counter holder
    float alphaBattery, redBattery, greenBattery, blueBattery, alphaFuel, redFuel, greenFuel, blueFuel;
    bool batteryHit = false, fuelHit = false;

    // Start is called before the first frame update
    void Start()
    {
        batteryPower = 500;
        fuelPower = 1000;
        selfRigidbody = GetComponent<Rigidbody2D>();

        if (transform.tag == "Battery")
        {
            selfSpriteBattery = GetComponent<SpriteRenderer>();
            alphaBattery = selfSpriteBattery.color.a;
            redBattery = selfSpriteBattery.color.r;
            greenBattery = selfSpriteBattery.color.g;
            blueBattery = selfSpriteBattery.color.b;
        }

        if (transform.tag == "FuleDramp")
        {
            selfSpriteFuel = GetComponent<SpriteRenderer>();
            alphaFuel = selfSpriteFuel.color.a;
            redFuel = selfSpriteFuel.color.r;
            greenFuel = selfSpriteFuel.color.g;
            blueFuel = selfSpriteFuel.color.b;
        }
            

        selfColider = GetComponent<CapsuleCollider2D>();

    }

    // Update is called once per frame
    void Update()
    {
        //for Battery
        if (batteryBar == null)
        {
            batteryBar = GameObject.FindGameObjectWithTag("Energy");
            BatterycounterChecker = batteryBar.GetComponentInChildren<BatteryBar>().batteryCounter;
            Debug.Log(batteryBar.tag);
        }
        //for fule
        if (fuleBar == null)
        {
            fuleBar = GameObject.FindGameObjectWithTag("Fule");
            FulecounterChecker = fuleBar.GetComponentInChildren<FuelBar>().fuelCounter;
            Debug.Log(batteryBar.tag);
        }
        if (batteryHit)
        {
            if (transform.tag == "Battery")
            {
                selfSpriteBattery.color = new Color(red, green, blue, alpha);
                alphaBattery = alphaBattery - .15f;
            }
        }
        if (fuelHit)
        {
            if (transform.tag == "FuleDramp")
            {
                selfSpriteFuel.color = new Color(red, green, blue, alpha);
                alphaFuel = alphaFuel - .15f;
            }
        }

    }
    //for all
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            selfRigidbody.isKinematic = true;
            transform.position = new Vector2(transform.position.x, transform.position.y + 4f);
            selfColider.isTrigger = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Car" && transform.tag == "Battery")
        {
            batteryPower = 500;
            selfRigidbody.isKinematic = false;
            selfRigidbody.gravityScale = -5f;
            batteryHit = true;
            BatterycounterChecker = batteryBar.GetComponentInChildren<BatteryBar>().batteryCounter;

            batteryBar.GetComponentInChildren<BatteryBar>().batteryCounter = 96f;

            Instantiate(transform, new Vector2(transform.position.x + 500f, GameObject.Find("LevelManager").transform.position.y), transform.rotation);
            Destroy(gameObject, .5f);
            //add barari

        }
        //fule 
        if (collision.gameObject.tag == "Car" && transform.tag == "FuleDramp")
        {
            fuelPower = 1000;
            selfRigidbody.isKinematic = false;
            selfRigidbody.gravityScale = -5f;
            fuelHit = true;
            FulecounterChecker = fuleBar.GetComponentInChildren<FuelBar>().fuelCounter;

            fuleBar.GetComponentInChildren<FuelBar>().fuelCounter = 96f;
            Instantiate(transform, new Vector2(transform.position.x + 500f, GameObject.Find("LevelManager").transform.position.y), transform.rotation);
            
            Destroy(gameObject, .5f);
            //add barari

        }

    }
}
