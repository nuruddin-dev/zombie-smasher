﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryBar : MonoBehaviour
{
    // Start is called before the first frame update
    SpriteRenderer selfSprite;
    float red, green, blue;
    public float batteryCounter = 1;

    void Start()
    {
        selfSprite = GetComponent<SpriteRenderer>();

        red = selfSprite.color.r;
        green = selfSprite.color.g;
        blue = selfSprite.color.b;
    }

    // Update is called once per frame
    void Update()
    {
        batteryCounter = Battery.batteryPower;

        if (batteryCounter > 0)
        {
            transform.localScale = new Vector2(batteryCounter / 500f, transform.localScale.y);
        }
        else batteryCounter = 1;

        if (transform.localScale.x <= 1f && transform.localScale.x > .5f)
        {
            //green
            selfSprite.color = new Color(0f, 255f, 0f);

        }
        if (transform.localScale.x <= .5f && transform.localScale.x > .2f)
        {
            //red green
            selfSprite.color = new Color(255f, 255f, 0f);
        }

        if (transform.localScale.x <= .2f && transform.localScale.x > 0f)
        {
            //red
            selfSprite.color = new Color(255f, 0f, 0f);
        }
        
    }


}
