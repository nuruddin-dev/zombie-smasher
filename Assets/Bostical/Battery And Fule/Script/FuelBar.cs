﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelBar : MonoBehaviour
{
    SpriteRenderer selfSprite;
    float red, green, blue;
    public float fuelCounter = 1;

    // Start is called before the first frame update
    void Start()
    {
        selfSprite = GetComponent<SpriteRenderer>();

        red = selfSprite.color.r;
        green = selfSprite.color.g;
        blue = selfSprite.color.b;
    }

    // Update is called once per frame
    void Update()
    {
        fuelCounter = Battery.fuelPower;

        if (fuelCounter > 0)
        {
            transform.localScale = new Vector2(fuelCounter / 1000f, transform.localScale.y);
        }
        else fuelCounter = 1;

        if (transform.localScale.x <= 1f && transform.localScale.x > .5f)
        {
            //green
            selfSprite.color = new Color(0f, 255f, 0f);

        }
        if (transform.localScale.x <= .5f && transform.localScale.x > .2f)
        {
            //red green
            selfSprite.color = new Color(255f, 255f, 0f);
        }

        if (transform.localScale.x <= .2f && transform.localScale.x > 0f)
        {
            //red
            selfSprite.color = new Color(255f, 0f, 0f);
        }
    }
}
